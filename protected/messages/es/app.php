<?php

return array(
    /* protected/controller/SiteController.php */
    'You have registered successfully' =>
    'Te has registrado exitosamente',

    'Error on displaying the message' =>
    'Error al mostrar el mensaje',

    'You can not accept the trade because the seller does not exist' =>
    'No puedes aceptar el trade porque el vendedor no existe',

    'You can not accept the trade because you do not have enough money' =>
    'No puedes aceptar el trade porque no tienes suficiente dinero',

    'That trade does not includes you' =>
    'El trade no te incluye',

    'That trade does not exist' =>
    'El trade no existe',

    'You do not have this item stock' =>
    'No tienes stock de ese objeto',

    'Your offer is insuficient' =>
    'Tu oferta es insuficiente',

    'You do not have gold' =>
    'No tienes oro',

    'That batch does not exist' =>
    'Esa puja no existe',

    'You do not have this item stock' =>
    'No tienes ese stock del objeto',

    'That item does not exist' =>
    'Ese objeto no existe',

    'You do not have this item' =>
    'No tienes ese objeto',

    'You have created successfully a clan named "{name}"' =>
    'Has creado exitosamente un clan llamado "{name}"',

    'Member "{name}" has left from the clan' =>
    'El miembro "{name}" ha salido del clan',

    'You has left from the "{name}" clan' =>
    'Has salido del clan "{name}"',

    'You can not leave from the clan if you are the leader' =>
    'No puedes salirte del clan si eres el líder',

    'That clan does not exist' =>
    'Ese clan no existe',

    'You are not member of any clan' =>
    'No eres miembro de ningún clan',

    'The character "{name}" was removed from the clan' =>
    'El jugador "{name}" fue removido del clan',

    'You have been removed from "{name}" clan' =>
    'Has sido removido del clan "{name}"',

    'Error: You can not remove yourself (leader) from the clan' =>
    'Error: No puedes removerte (lider) del clan',

    'Error: That user does not belongs to the clan' =>
    'Error: Ese usuario no pertenece al clan',

    'Error: You are not the clan leader!. Only clan leaders can reject petitions' =>
    'Error: ¡No eres el líder del clan!. Solo los líderes de clan pueden declinar peticiones',

    'Error: That clan does not exist' =>
    'Error: Ese clan no existe',

    'Error: That user does not exist' =>
    'Error: Ese usuario no existe',

    'The petition made by the character "{name}" was rejected' =>
    'La petición hecha por el jugador "{name}" fue rechazada',

    'Your petition to join into "{name}" clan was rejected' =>
    'Tu petición para ingresar al clan "{name}" fue rechazada',

    'The character "{name}" was correctly accepted into the clan' =>
    'El jugador {name} fue correctamente aceptado en el clan',

    'A new member ("{name}") has been accepted into the clan!' =>
    '¡Un nuevo miembro ("{name}") ha sido aceptado en el clan!',

    'You have been accepted into "{name}" clan!' =>
    '¡Has sido aceptado en el clan "{name}"!',

    'Error: There are too many members into the clan, you can not accept new members' =>
    'Error: Hay muchos miembros en el clan, no puedes aceptar nuevos miembros',

    'Error: You are not the clan leader!. Only clan leaders can accept new members' =>
    'Error: ¡No eres el líder del clan!. Solo los líderes de clan pueden aceptar nuevos miembros',

    'Error: That character does not exist or already has a clan' =>
    'Error: El jugador no existe o ya tiene un clan',

    'You have sent a petition to join into "{name}" clan' =>
    'Has enviado una petición para entrar al clan "{name}"',

    'New petition to join into the clan by the character "{name}"' =>
    'Nueva petición para ingresar al clan hecha por el jugador "{name}"',

    'Error: That clan cannot accept new members because it is full' =>
    'Error: Ese clan no puede aceptar nuevos miembros porque está lleno',

    'Error: You have already sent a petition to that clan, wait for answer' =>
    'Error: Ya has enviado una petición a ese clan, espera la respuesta',

    'Error: You already are in a clan' =>
    'Error: Ya estás en un clan',

    'You accept the quest: <b>{name}</b>' =>
    'Has aceptado la misión: <b>{name}</b>',

    'You buy {amount} {name}' =>
    'Compraste {amount} {name}',

    '{amount} "{name}" have been deleted from your inventory' =>
    '{amount} "{name}" han sido borrados de tu inventario',

    '"{name}" has been deleted from your inventory' =>
    '"{name}" ha sido borrado de tu inventario',

    'You have been used {amount} of "{name}"' =>
    'Has usado {amount} de "{name}"',

    'Error: You do not have that amount of "{name}"' =>
    'Error: No tienes esa cantidad de "{name}"',

    'Error: That item is not an a consumable' =>
    'Error: Ese objeto no es consumible',

    'Error: That consumable does not exist' =>
    'Error: Ese consumible no existe',

    /* protected/views/site/_notifications.php */
    'Notifications' =>
    'Notificaciones',

    /* protected/views/site/_timers.php */
    'Timers' =>
    'Temporizadores',

    /* protected/views/site/auction.php */
    'Auctions' =>
    'Subastas',

    'Select item' =>
    'Selecciona el objeto',

    'Auction' =>
    'Subastar',

    /* protected/views/site/createClan.php */
    'Create clan' =>
    'Crear clan',

    /* protected/views/site/general.php */
    'Left time...' =>
    'Tiempo restante...',

    'You are not member of any clan, join one!' =>
    'No eres miembro de ningún clan, ¡ingresa a uno!',

    'You are member of {name} clan' =>
    'Eres miembro del clan {name}',

    'Stats' =>
    'Estadísticas',

    'Damage' =>
    'Daño',

    'Defense' =>
    'Defensa',

    'Armor penetration' =>
    'Penetración de armadura',

    'Evasion' =>
    'Evasión',

    'Luck' =>
    'Suerte',

    'Life' =>
    'Vida',

    'Constitution' =>
    'Constitución',

    'Accuracy' =>
    'Precisión',

    'Inventory' =>
    'Inventario',

    'Use consumable' =>
    'Usar consumible',

    'Drop item' =>
    'Tirar objeto',

    'You do not have objects in your inventory' =>
    'No tienes objetos en tu inventario',

    'Weapon' =>
    'Arma',

    '(Save weapon in inventory)' =>
    '(Guardar arma en inventario)',

    'Attack' =>
    'Ataque',

    'You do not have weapon' =>
    'No tienes arma',

    'Shield' =>
    'Escudo',

    '(Save shield in inventory)' =>
    '(Guardar escudo en inventario)',

    'You do not have shield' =>
    'No tienes escudo',

    'Companion' =>
    'Acompañante',

    'You do not have companion' =>
    'No tienes acompañante',
);