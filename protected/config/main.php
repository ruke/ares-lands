<?php

return array(
    'basePath'  => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name'      => 'AresLands',
    'language'  => 'es',
    'preload'   => array('log'),
    'import'    => array(
        'application.models.*',
        'application.components.*',
        'application.components.triggers.*',
        'application.components.triggers.quests.*',
    ),
    'modules'   => array(
        'giic'  => array(
            'class'     => 'system.gii.GiiModule',
            'password'  => '1234',
            'ipFilters' => array('127.0.0.1', '::1')
        )
    ),

    'components'=> array(
        'user'          => array(
            'allowAutoLogin'    => false,
        ),

        'request'       => array(
            'enableCsrfValidation'  => true,
            'enableCookieValidation'=> true
        ),
        
        'urlManager'    => array(
            'urlFormat'        => 'path',
            'showScriptName'   => false,
            'rules'            => array(
                '<controller:\w+>/<id:\d+>'                => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'   => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>'            => '<controller>/<action>'
            )
        ),
        
        'db'            => array(
            'class'             => 'CDbConnection',
            'connectionString'  => 'mysql:host=localhost;dbname=areslands',
            'emulatePrepare'    => true,
            'username'          => 'root',
            'password'          => '',
            'charset'           => 'utf8'
        ),

        'coreMessages'  => array(
            'basePath'  => 'protected/messages'
        ),
        
        'errorHandler'  => array(
            'errorAction'   => 'site/error'
        ),
        
        'log'           => array(
            'class'     => 'CLogRouter',
            'routes'    => array(
                array(
                    'class'     => 'CFileLogRoute',
                    'levels'    => 'error, warning'
                )
            )
        )
    ),
    'params'    => array(
        'adminEmail'    => 'contacto@areslands.com.ar',
    )
);