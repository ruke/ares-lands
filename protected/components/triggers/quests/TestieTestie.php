<?php

class TestieTestie extends AbstractQuest
{
    protected $id = 2;

    public function condition()
    {
        return (int) Events::$ACCEPTED_QUEST->id === (int) $this->id;
    }

    public function action()
    {
        $this->process->process = serialize(array('tip' => 'Colocate el arma para completar la mision', 'count' => 0, 'countFinish' => 1));
        $this->process->save();

        $notification = new Notifications();
        $notification->idCharacter = (int) Yii::app()->user->getId();
        $notification->content = 'You accepted quest!';
        $notification->save();
    }
}