<?php

class TormentosOscuros extends AbstractQuest
{
    protected $id = 1;

    public static function reward()
    {
        $character = Yii::app()->user->character;

        if (is_object($character)) {
            $character->username = 'asd';
            $character->save();
        }
    }

    public function conditionOnAcceptQuest()
    {
        return (int) Events::$ACCEPTED_QUEST->id === (int) $this->id;
    }

    public function actionOnAcceptQuest()
    {
        $this->getQuest();
        $this->getProcess();

        $this->process->process = serialize(array('tip' => 'Colocate un arma cualquiera 5 veces', 'count' => 0, 'countFinish' => 5));
        $this->process->save();
    }

    public function conditionOnEquipWeapon()
    {
        $this->getQuest();
        $this->getProcess();

        if (!is_object($this->quest) || !is_object($this->process)) {
            return false;
        }

        return $this->hasQuest() && !$this->isFinished();
    }

    public function actionOnEquipWeapon()
    {
        $notification = new Notifications();
        $notification->idCharacter = (int) Yii::app()->user->getId();

        $this->process->process['count'] = $this->process->process['count'] + 1;

        if ($this->process->process['count'] > $this->process->process['countFinish']) {
            $notification->content = 'You have been finished the quest "' . $this->quest->name . '"!';
            $this->process->process = 'REWARD';
        } else {
            $notification->content = $this->quest->name . ': ' . $this->process->process['count'] . '/' . $this->process->process['countFinish'];
            $this->process->process = serialize($this->process->process);
        }

        $notification->save();
        $this->process->save();
    }
}