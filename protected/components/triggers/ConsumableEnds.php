<?php

class ConsumableEnds
{
    public $timer;

    public function condition()
    {
        return true;
    }

    public function action()
    {
        $effect = Effects::model()->findByAttributes(array(
            'idCharacter'   => $this->timer->idCharacter,
            'idTimer'       => $this->timer->id,
        ));

        if (is_object($effect)) {
            $item = Items::model()->findByPk($effect->idItem);

            if (is_object($item)) {
                $notification = new Notifications();
                $notification->idCharacter = $this->timer->idCharacter;
                $notification->content = 'The effect of the "' . $item->name . '" consumable is over';

                $notification->save();
            }

            $effect->delete();
            $this->timer->delete();
        }
    }
}