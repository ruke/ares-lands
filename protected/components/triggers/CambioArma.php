<?php

class CambioArma extends AbstractQuest
{
    protected $id = 1;

    public function condition()
    {
        return $this->hasQuest() && !$this->isFinished();
    }

    public function action()
    {
        if (!is_array($this->process->process)) {
            $this->process->process = array();
            $this->process->process['count'] = 0;
        }

        $this->process->process['count'] = $this->process->process['count'] + 1;

        if ($this->process->process['count'] > 4) {
            $this->process->process = 'FINISHED';
        } else {
            $this->process->process = serialize($this->process->process);
        }

        $this->process->save();
    }
}