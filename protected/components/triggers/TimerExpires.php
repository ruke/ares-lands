<?php

class TimerExpires
{
    public function condition()
    {
        return true;
    }

    public function action()
    {
        foreach (Events::$EXPIRED_TIMERS as $timer) {
            $timerClass = new $timer->className;
            $timerClass->timer = $timer;

            if ($timerClass->condition()) {
                $timerClass->action();
            }
        }

        Events::$EXPIRED_TIMERS = array();
    }
}