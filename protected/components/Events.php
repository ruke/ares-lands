<?php

class Events
{
    const LOGIN_EVENT           = 1;
    const TALK_TO_NPC_EVENT     = 2;
    const BATTLE_START_EVENT    = 3;
    const BATTLE_END_EVENT      = 4;
    const CHANGE_WEAPON_EVENT   = 5;
    const CHANGE_SHIELD_EVENT   = 6;
    const CHANGE_COMPANION_EVENT= 7;
    const ACCEPT_QUEST_EVENT    = 8;
    const TIMER_EXPIRES_EVENT   = 9;

    public static $EXPIRED_TIMERS = array();
    public static $ACCEPTED_QUEST;

    private static $triggers = array(
        self::LOGIN_EVENT           => array(),

        self::TALK_TO_NPC_EVENT     => array(),

        self::BATTLE_START_EVENT    => array(),

        self::BATTLE_END_EVENT      => array(),

        self::CHANGE_WEAPON_EVENT   => array(
            array(
                'className' => 'TormentosOscuros',
                'condition' => 'conditionOnEquipWeapon',
                'action'    => 'actionOnEquipWeapon',
            ),
        ),

        self::CHANGE_SHIELD_EVENT   => array(),

        self::CHANGE_COMPANION_EVENT=> array(),

        self::ACCEPT_QUEST_EVENT    => array(
            array(
                'className' => 'TormentosOscuros',
                'condition' => 'conditionOnAcceptQuest',
                'action'    => 'actionOnAcceptQuest',
            ),

            array(
                'className' => 'TestieTestie',
                'condition' => 'condition',
                'action'    => 'action',
            ),
        ),

        self::TIMER_EXPIRES_EVENT   => array(
            array(
                'className' => 'TimerExpires',
                'condition' => 'condition',
                'action'    => 'action',
            ),
        ),
    );

    public static function fire($eventId)
    {
        foreach (self::$triggers[$eventId] as $trigger) {
            $instance = new $trigger['className']();

            if ($instance->$trigger['condition']()) {
                $instance->$trigger['action']();
            }
        }
    }
}