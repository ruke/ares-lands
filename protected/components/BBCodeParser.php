<?php

/**
* BBCodeParser is a parser from bbcode to safe html
*
* @author Franco Montenegro
*/
class BBCodeParser
{
    private $_text;
    private static $_bbcodes = array(
        '/\[b\](.*?)\[\/b\]/',

        '/\[left\](.*?)\[\/left\]/',
        '/\[center\](.*?)\[\/center\]/',
        '/\[right\](.*?)\[\/right\]/',

        '/\[img( width=([1-9][0-9]))?( height=([1-9][0-9]))?\](.*?)\[\/img\]/',

        '/\[size=(1[0-9]|2[0-4])\](.*?)\[\/size\]/',

        '/\[url=(.*?)\](.*?)\[\/url\]/',        
    );

    private static $_html = array(
        '<b>$1</b>',

        '<div style="text-align: left;">$1</div>',
        '<div style="text-align: center;">$1</div>',
        '<div style="text-align: right;">$1</div>',

        '<img width="$2px" height="$4px" src="$5" />',

        '<span style="font-size: $1px">$2</span>',

        '<a href="$1">$2</a>',
    );

    private static $_group = array(
        '$1',
        
        '$1',
        '$1',
        '$1',

        '$1',

        '$5',

        '$2',
    );

    public function __construct($text)
    {
        $this->_text = CHtml::encode($text);
    }

    public function getText()
    {
        return $this->_text;
    }

    public function cleanBbcode()
    {
        $this->_text = preg_replace(self::$_bbcodes, self::$_group, $this->_text);
        return $this;
    }

    public function parseBbcode()
    {
        $this->_text = preg_replace(self::$_bbcodes, self::$_html, $this->_text);
        return $this;
    }
}