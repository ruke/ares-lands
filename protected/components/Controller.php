<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();

    public $notifications;
    public $npcs;
    public $timers;

    public function beforeRender($view)
    {
        if (!Yii::app()->user->isGuest) {
            $character = Yii::app()->user->character;

            if (Yii::app()->user->character->showNotifications) {
                $this->notifications = Notifications::model()->findAllByAttributes(array('idCharacter' => $character->id));
            }

            $this->npcs = Npcs::model()->findAllByAttributes(array(
                'idZone'    => $character->idZone,
                'type'      => Npcs::NPC_TYPE,
            ));

            $now = time();
            $this->timers = Timers::model()->findAllByAttributes(array('idCharacter' => $character->id));

            if (count($this->timers) > 0) {
                foreach ($this->timers as $timer) {
                    if ($now >= (int) $timer->ends) {
                        Events::$EXPIRED_TIMERS[] = $timer;
                    }
                }

                if (count(Events::$EXPIRED_TIMERS) > 0) {
                    Events::fire(Events::TIMER_EXPIRES_EVENT);
                }
            }
        }

        return true;
    }

    public function afterRender($view)
    {
        if (!Yii::app()->user->isGuest && Yii::app()->user->character->showNotifications && count($this->notifications) > 0) {
            foreach ($this->notifications as $notification) {
                $notification->delete();
            }
        }

        return true;
    }
}