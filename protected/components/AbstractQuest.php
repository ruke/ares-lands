<?php

abstract class AbstractQuest
{
    protected $id;
    protected $quest;
    protected $process;

    public function getId()
    {
        return $this->id;
    }

    public function getQuest()
    {
        $this->quest = Quests::model()->findByAttributes(array(
            'id' => $this->id,
        ));

        return $this->quest;
    }

    public function getProcess()
    {
        $this->process = QuestsProcess::model()->findByAttributes(array(
            'idQuest'       => $this->quest->id,
            'idCharacter'   => (int) Yii::app()->user->getId(),
        ));

        if (is_object($this->process) && $this->process->process !== 'FINISHED') {
            $this->process->process = unserialize($this->process->process);
        }

        return $this->process;
    }

    public function hasQuest()
    {
        return is_object($this->process);
    }

    public function isFinished()
    {
        return $this->process->process === 'FINISHED';
    }
}