<?php

class UserIdentity extends CUserIdentity
{
    protected $_id;
    public $idServer;

    public function authenticate()
    {
        $user = Characters::model()->findByAttributes(array('username' => $this->username, 'idServer' => $this->idServer));

        if ($user === null) {
            $this->errorCode = self::ERROR_USERNAME_INVALID;
        } else if ($user->password !== md5($this->password)) {
            $this->errorCode = self::ERROR_PASSWORD_INVALID;
        } else {
            $this->_id = $user->id;
            $this->setState('character', $user);
            
            $this->errorCode = self::ERROR_NONE;
        }
    }
 
    public function getId()
    {
        return $this->_id;
    }
}