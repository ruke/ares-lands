<?php if (Yii::app()->user->hasFlash('trade')) { ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('trade'); ?>
            </div>
<?php } elseif (Yii::app()->user->hasFlash('tradeError')) { ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('tradeError'); ?>
            </div>
<?php } else { ?>
            <?php $this->redirect(array('site/')); ?>
<?php } ?>