<?php
if (Yii::app()->user->hasFlash('create')) {
?>
    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('create'); ?>
    </div>
<?php
} else {
?>
    <div class="form">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'                        => 'create-clan-form',
            'enableClientValidation'    => true,
            'clientOptions'             => array('validateOnSubmit' => true),
            'htmlOptions'               => array('autocomplete' => 'off'),
        ));

        echo $form->errorSummary($clanForm);
        ?>

        <div class="row">
            <?php 
                echo $form->labelEx($clanForm, 'name');
                echo $form->textField($clanForm, 'name', array('maxlength' => 100));
                echo $form->error($clanForm, 'name'); 
            ?>
        </div>

        <div class="row">
            <?php 
                echo $form->labelEx($clanForm, 'description');
                echo $form->textArea($clanForm, 'description');
                echo $form->error($clanForm, 'description'); 
            ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('app', 'Create clan'), array('class' => 'input-button')); ?>
        </div>

        <?php
        $this->endWidget();
        ?>
    </div>
<?php
}
?>