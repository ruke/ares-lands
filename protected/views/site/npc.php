<?php
if (!is_object($npc)) {
    $this->redirect(array('site/index'));
}
?>

<div class="left" style="padding-right: 10px; width: 280px;">
    <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/pers/<?php echo $npc->id; ?>.jpg" /></center>
</div>

<div class="right" style="width: 600px;">
    <div class="dark-content">
        <h2><?php echo $npc->name; ?></h2>
        <p><?php echo $npc->description; ?></p>

        <div style="margin-top: 20px">
            <h5>Quests</h5>

            <?php
            $countQuest = 0;
            foreach ($quests as $quest) {
                $questProcess = QuestsProcess::model()->findByAttributes(array('idQuest' => (int) $quest->id, 'idCharacter' => (int) $character->id));
                if ($quest->minLevel > $character->level || (is_object($questProcess) && $questProcess->process === 'FINISHED')) {
                    continue;
                }

                $countQuest++;
            ?>
                <div class="light-content" style="margin-top: 5px;">
                    <div name="menu" tooltip="clic para mas info">
                        <b><?php echo $quest->name; ?></b>
                        <div name="menuInnerContent">
                            <p><?php echo $quest->description; ?></p>

                            <?php
                            if (!is_object($questProcess)) {
                            ?>
                                <?php echo CHtml::link('Accept quest', array('site/acceptQuest', 'id' => $quest->id)); ?>
                            <?php
                            } else {
                                if ($questProcess->process === 'REWARD') {
                                    echo CHtml::link('Claim Reward', array('site/claimQuestReward', 'idQuest' => $quest->id));
                                } else {
                                    $questProcess->process = unserialize($questProcess->process);

                                    echo $questProcess->process['tip'] . '<br />';

                                    if (isset($questProcess->process['count']) && isset($questProcess->process['countFinish'])) {
                                        echo '<b>Process:</b> ' . $questProcess->process['count'] . '/' . $questProcess->process['countFinish'];
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            <?php
            }

            if ($countQuest === 0) {
                echo 'You do not have any quests';
            }
            ?>
        </div>
    </div>
</div>

<div class="clear"></div>

<div style="margin-top: 20px;">
    <h5>Items</h5>

    <?php
    if (count($itemsToSell) > 0) {
        foreach ($itemsToSell as $item) {
            $gold   = substr($item->goldPrice, 0, -4) ? substr($item->goldPrice, 0, -4) : 0;
            $silver = substr($item->goldPrice, -4, -2) ? substr($item->goldPrice, -4, -2) : 0;
            $copper = substr($item->goldPrice, -2) ? substr($item->goldPrice, -2) : 0;
    ?>
            <?php 
            echo CHtml::beginForm(array('site/buyItem')); 
            echo CHtml::hiddenField('name', $item->name);
            ?>
            <div style="padding-top: 20px;">
                <div class="left" style="width: 190px;">
                    <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $item->id; ?>.jpg" /></center>
                </div>
                
                <div class="right" style="width: 700px;">
                    <b><?php echo $item->name; ?></b>

                    <?php if ($item->damage != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Attack:</b></div>
                            <div class="right"><?php echo $item->damage; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->defense != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Defense:</b></div>
                            <div class="right"><?php echo $item->defense; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->armorPenetration != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Armor penetration:</b></div>
                            <div class="right"><?php echo $item->armorPenetration; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->evasion != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Evasion:</b></div>
                            <div class="right"><?php echo $item->evasion; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->luck != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Luck:</b></div>
                            <div class="right"><?php echo $item->luck; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->life != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Life:</b></div>
                            <div class="right"><?php echo $item->life; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->constitution != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Constitution:</b></div>
                            <div class="right"><?php echo $item->constitution; ?></div>
                        </div>
                    <?php } ?>

                    <?php if ($item->accuracy != 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Accuracy:</b></div>
                            <div class="right"><?php echo $item->accuracy; ?></div>
                        </div>
                    <?php } ?>

                    <?php if (((int) $item->type === Items::POTION_TYPE || (int) $item->type === Items::STONE_TYPE) && $item->goldPrice > 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Quantity:</b></div>
                            <div class="right">
                                <?php
                                $amount = array();
                                if ((int) @($character->gold / $item->goldPrice) > 0) {
                                    for ($i = 1, $max = (int) @($character->gold / $item->goldPrice); $i <= $max; ++$i) {
                                        if ($i > 25) {
                                            $i = $i + 4;
                                        }

                                        if ($i > 50) {
                                            break;
                                        }

                                        $amount[$i] = $i;
                                    }
                                } else {
                                    $amount[0] = 0;
                                }
                                echo CHtml::dropDownList('amount', 'amount', $amount);
                                ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b>Price:</b></div>
                        <div class="right">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m3.gif" /> <?php echo $gold; ?>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m2.gif" /> <?php echo $silver; ?>
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m1.gif" /> <?php echo $copper; ?>
                        </div>
                    </div>

                    <?php if ($item->vipPrice > 0) { ?>
                        <div class="dark-content" style="height: 10px;">
                            <div class="left"><b>Vip price:</b></div>
                            <div class="right">
                                <?php echo $item->vipPrice; ?>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="right" style="margin-top: 10px; font-size: 16px;">
                        <?php
                        echo CHtml::linkButton('Buy', array('params' => array('normalCoin' => 'normalCoin')));
                        ?>

                        <?php if ($item->vipPrice > 0 && $character->vipCoins >= $item->vipPrice) { ?>
                            or <?php echo CHtml::linkButton('Buy with VIP coins', array('params' => array('vipCoin' => 'vipCoin'))); ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="clear separator-bar" style="background-position: 0 50%; height: 50px;"></div>
            </div>
            <?php
            //$this->endWidget(); 
            echo CHtml::endForm(); 
            ?>
    <?php
        }
    }
    ?>
</div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tooltip.js"></script>
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/menu.js"></script>