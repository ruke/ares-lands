<div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/a8.jpg" height="30px" widht="30px" /></div>
<h1>Elije una raza para continuar</h1>
<p>Recuerda que cada clase tiene caracteristicas que las distinguen de las demas. Elije con cuidado y sabiamente.</p>

<div style="margin-top: 50px;">
    <a href="#"><div class="left"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/1_0_378.jpg" /></div></a>
    <div class="right" style="margin-top: 50px;">
        <div style="width: 600px;" class="dark-content">
            <div class="left" style="padding: 5px; margin-top: -10px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/enano_ico.jpg" /></div>
            <h3><?php echo CHtml::link(Yii::t('app', 'Dwarfs'), array('site/register', 'raceId' => 1)); ?></h3>
            <p>Los enanos son criaturas curiosas pero tambien muy salvajes, quienes se alzan con la victoria lanzando brutos ataques fisicos a sus adversarios, siendo que no les importa mucho su defensa. Ellos lucharan con todo su poder para salir victoriosos de un encuentro.</p>
            
            <h5>Pros</h5>
            <ul>
                <li>El enano como guerrero nato, tiene la habilidad de hacer mucho daño fisico</li>
                <li>Grandes cantidades de vida</li>
            </ul>

            <h5>Contras</h5>
            <ul>
                <li>Muy vulnerable a ataques magicos</li>
            </ul>

            <div class="right"><?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/images/jugar-como-enano.png" />', array('site/register', 'raceId' => 1)); ?></div>
        </div>
    </div>
</div>

<div class="clear"></div>

<div style="margin-top: 50px;">
    <a href="#"><div class="right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/2_0_13826.jpg" /></div></a>
    <div class="left" style="margin-top: 100px;">
        <div style="width: 600px;" class="dark-content">
            <div class="left" style="padding: 5px; margin-top: -10px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/elfo_ico.jpg" /></div>
            <h3><?php echo CHtml::link(Yii::t('app', 'Elfs'), array('site/register', 'raceId' => 2)); ?></h3>
            <p>Los elfos son criaturas misticas muy reservadas. No se conoce mucho de ellos, pero si se sabe que se debe tener mucho cuidado al enfrentar a uno de ellos. Segun se dice, estas criaturas poseen grandes destrezas tanto en combate fisico como en magico.</p>
        
            <h5>Pros</h5>
            <ul>
                <li>Los elfos son maestros del combate fisico y magico</li>
                <li>Poseen gran evasion</li>
            </ul>

            <h5>Contras</h5>
            <ul>
                <li>No son muy resistentes a los ataques</li>
            </ul>

            <div class="left"><?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/images/jugar-como-elfo.png" />', array('site/register', 'raceId' => 2)); ?></div>
        </div>
    </div>
</div>

<div class="clear"></div>

<div style="margin-top: 50px;">
    <a href="#"><div class="left"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/3_1_269.jpg" /></div></a>
    <div class="left" style="margin-top: 100px;">
        <div style="width: 600px;" class="dark-content">
            <div class="left" style="padding: 5px; margin-top: -10px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/drow_ico.jpg" /></div>
            <h3><?php echo CHtml::link(Yii::t('app', 'Drows'), array('site/register', 'raceId' => 3)); ?></h3>
            <p>Se rumorea que los Drows hicieron pactos oscuros para obtener sus grandiosos poderes. Ellos son maestros en el arte de la magia y las maldiciones, ¡por lo que deberas ser muy cuidadoso al enfrentarlos!.</p>
        
            <h5>Pros</h5>
            <ul>
                <li>Gran daño magico</li>
                <li>Mayor probabilidad de acestar un golpe critico</li>
            </ul>

            <h5>Contras</h5>
            <ul>
                <li>No son muy resistentes a los ataques</li>
            </ul>

            <div class="right"><?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/images/jugar-como-drow.png" />', array('site/register', 'raceId' => 3)); ?></div>
        </div>
    </div>
</div>

<div class="clear"></div>

<div style="margin-top: 50px;">
    <div class="right"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/4_1_377.jpg" /></div>
    <div class="left" style="margin-top: 150px;">
        <div style="width: 600px;" class="dark-content">
            <div class="left" style="padding: 5px; margin-top: -10px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/humano_ico.jpg" /></div>
            <h3><?php echo CHtml::link(Yii::t('app', 'Humans'), array('site/register', 'raceId' => 4)); ?></h3>
            <p>Los humanos son criaturas traicioneras a las que no debes darle la espalda, ¡no dudaran en atacarte!. Bueno, aunque no todos son asi, varios son dignos del respeto del mas grande guerrero o mago.</p>
        
            <h5>Pros</h5>
            <ul>
                <li>Gran daño critico</li>
                <li>Poseen una armadura mediana</li>
            </ul>

            <h5>Contras</h5>
            <ul>
                <li>No poseen mucho ataque</li>
            </ul>

            <div class="left"><?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/images/jugar-como-humano.png" />', array('site/register', 'raceId' => 4)); ?></div>
        </div>
    </div>
</div>