<h2><?php echo Yii::t('app', 'Auctions'); ?></h2>

<div class="form">

    <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id'                        => 'auction-form',
            'enableClientValidation'    => true,
            'clientOptions'             => array('validateOnSubmit' => true),
            'htmlOptions'               => array('autocomplete' => 'off'),
        ));
    ?>
    
        <div class="row">
            <?php
                $itemInfo = array();
                $item;
                foreach ($inventoryItems as $inventoryItem) {
                    $item = Items::model()->findByPk($inventoryItem->idItem);
                    $itemInfo[$item->id] = $item->name;
                }

                echo $form->labelEx($auctionForm, 'idItem');
                echo $form->dropDownList($auctionForm, 'idItem', $itemInfo, array(
                    'empty' => Yii::t('app', 'Select item'),
                    'ajax'  => array(
                        'type'  => 'POST',
                        'url'   => array('site/GetItemAmount'),
                        'update'=> '#' . CHtml::activeId($auctionForm, 'amount'),
                    ),
                ));
                echo $form->error($auctionForm, 'idItem');
            ?>
        </div>

        <div class="row">
            <?php
                echo $form->labelEx($auctionForm, 'amount');
                echo $form->dropDownList($auctionForm, 'amount', array(0));
                echo $form->error($auctionForm, 'amount');
            ?>
        </div>

        <div class="row">
            <?php
                echo $form->labelEx($auctionForm, 'price');
                echo $form->textField($auctionForm, 'price');
                echo $form->error($auctionForm, 'price');
            ?>
        </div>

        <div class="row">
            <?php
                echo $form->labelEx($auctionForm, 'cycles');
                echo $form->dropDownList($auctionForm, 'cycles', array(1, 2, 3, 4));
                echo $form->error($auctionForm, 'cycles');
            ?>
        </div>

        <div class="row buttons">
            <?php echo CHtml::submitButton(Yii::t('app', 'Auction')); ?>
        </div>

    <?php $this->endWidget(); ?>

</div>