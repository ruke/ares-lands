<?php echo CHtml::link(Yii::t('app', 'Send a new private message'), array('site/sendPrivateMessage')); ?>

<?php foreach ($messages as $message) { ?>
    <?php $sender = Characters::model()->findByPk($message->idFrom); ?>
    <div class="dark-content">
        <div><b>Subject:</b> <?php echo CHtml::encode($message->messageSubject); ?></div>
        <div><b>Sender:</b> <?php echo $sender->username; ?></div>
        <div class="right" style="margin-top: -25px;">
            <?php echo CHtml::link('Read', array('site/messages', 'id' => $message->id)); ?>
            <?php echo CHtml::link('Delete', array('site/deleteMessage', 'id' => $message->id)); ?>
        </div>
    </div>
<?php } ?>