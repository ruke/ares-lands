<?php
    $this->pageTitle = Yii::app()->name . ' - ' . Yii::t('app', 'Register');
?>

<h1><?php echo Yii::t('app', 'Register'); ?></h1>

<?php if (Yii::app()->user->hasFlash('register')) { ?>

    <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('register'); ?>
    </div>

<?php } else { ?>

    <div class="form">

        <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id'                        => 'register-form',
                'enableClientValidation'    => true,
                'clientOptions'             => array('validateOnSubmit' => true),
                'htmlOptions'               => array('autocomplete' => 'off'),
            ));
        ?>

            <p class="note"><?php echo Yii::t('app', 'Fields with'); ?> <span class="required">*</span> <?php echo Yii::t('app', 'are required'); ?>.</p>

            <?php echo $form->errorSummary($registerForm); ?>

            <div class="row">
                <?php 
                    echo $form->labelEx($registerForm, 'username');
                    echo $form->textField($registerForm, 'username', array('maxlength' => 100));
                    echo $form->error($registerForm, 'username'); 
                ?>
            </div>

            <div class="row">
                <?php 
                    echo $form->labelEx($registerForm, 'password');
                    echo $form->passwordField($registerForm, 'password', array('maxlength' => 40));
                    echo $form->error($registerForm, 'password'); 
                ?>
            </div>

            <div class="row">
                <?php 
                    echo $form->labelEx($registerForm, 'passwordRepeat');
                    echo $form->passwordField($registerForm, 'passwordRepeat', array('maxlength' => 40));
                    echo $form->error($registerForm, 'passwordRepeat'); 
                ?>
            </div>

            <div class="row">
                <?php 
                    echo $form->labelEx($registerForm, 'email');
                    echo $form->textField($registerForm, 'email');
                    echo $form->error($registerForm, 'email'); 
                ?>
            </div>

            <div class="row">
                <?php 
                    echo $form->labelEx($registerForm, 'emailRepeat');
                    echo $form->textField($registerForm, 'emailRepeat');
                    echo $form->error($registerForm, 'emailRepeat'); 
                ?>
            </div>
            
            <div class="row">
                <?php
                    echo $form->labelEx($registerForm, 'gender');
                    echo $form->dropDownList($registerForm, 'gender', array(0 => Yii::t('app', 'Male'), 1 => Yii::t('app', 'Female')));
                    echo $form->error($registerForm, 'gender');
                ?>
            </div>
            
            <div class="row">
                <?php
                    echo $form->labelEx($registerForm, 'idLanguage');
                    echo $form->dropDownList($registerForm, 'idLanguage', array(1 => Yii::t('app', 'Spanish'), 2 => Yii::t('app', 'English')));
                    echo $form->error($registerForm, 'idLanguage');
                ?>
            </div>

            <div class="row">
                <?php
                    echo $form->labelEx($registerForm, 'idServer');
                    echo $form->dropDownList($registerForm, 'idServer', array(0 => Yii::t('app', 'Server 1')));
                    echo $form->error($registerForm, 'idServer');
                ?>
            </div>

            <?php if (CCaptcha::checkRequirements()) { ?>
                <div class="row">
                    <?php echo $form->labelEx($registerForm, 'verifyCode'); ?>

                    <div>
                    <?php $this->widget('CCaptcha', array('buttonLabel' => '<br />' . Yii::t('yii', 'Get a new code'))); ?>
                    <div><?php echo $form->textField($registerForm,'verifyCode'); ?></div>
                    </div>

                    <div class="hint">
                        <?php echo Yii::t('app', 'Please enter the letters as they are shown in the image above'); ?>.
                        <br/>
                        <?php echo Yii::t('app', 'Letters are not case-sensitive'); ?>.
                    </div>

                    <?php echo $form->error($registerForm, 'verifyCode'); ?>
                </div>
            <?php } ?>
            
            <div class="row rememberMe">
                <?php 
                    echo $form->checkBox($registerForm, 'acceptTermsAndConditions');
                    echo $form->labelEx($registerForm, 'acceptTermsAndConditions');
                    echo $form->error($registerForm, 'acceptTermsAndConditions'); 
                ?>
            </div>

            <div class="row buttons">
                <?php echo CHtml::submitButton(Yii::t('app', 'Register'), array('class' => 'input-button')); ?>
            </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->

<?php } ?>