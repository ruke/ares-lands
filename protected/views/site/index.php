<h1>Bienvenido a <b>AresLands</b></h1>
<div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/14116.jpg" /></div>
<p><b>AresLands</b> es un <b>juego MMO que puedes jugar completamente gratis desde tu navegador</b> (no necesitas descargar ni instalar nada para poder jugarlo).<br />
Afilen sus armas y encanten sus varitas como nunca, porque una nueva y mejorada version del antiguo Tierras de Leyendas por fin ha llegado.</p>

<h1><b>¿Que encontrare en AresLands?</b></h1>

<ul>
    <li>
        <h2><a href="javascript:document.getElementById('jugadorAntiguo').style.display = 'block';">¿Antiguo jugador de Tierras de Leyenda?</a></h2>
        <div id="jugadorAntiguo" style="display: none;"><p>¿Cansado de unicamente explorar?, ¿cansado de la monotonia?, ¿cansado de no encontrar nada nuevo?. Ya no te angusties mas, prueba AresLands!</p></div>
    </li>

    <li>
        <h2>Batallas entre clanes</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/ataque.jpg" height="30px" widht="30px" /></div>
        <p>Todas las semanas se realizan luchas entre clanes para determinar cual es el mejor y otorgarle su respectiva recompensa. ¿Acaso seras tu el que cree al clan mas epico y logre hacerse con el trono?</p>
    </li>

    <li>
        <h2>Mazmorras de clan</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/1.jpg" height="30px" widht="30px" /></div>
        <p>Si has logrado salir victorioso de las batallas semanales, entonces tendras permitido entrar junto con tu clan a enfrentarte a los grandes desafios que esconden las mazmorras de AresLands para hacerte de grandes botines.
    </li>

    <li>
        <h2>Los dignos, ¡al trono!</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/a2.jpg" height="30px" widht="30px" /></div>
        <p>Aquellos grupos (clanes) que hayan logrado entrar dos veces concecutivas a las mazmorras seran recompensados con la corona de AresLands, la cual le otorgara el poder de reinar en el juego.</p>
    </li>

    <li>
        <h2>Cientos de objetos</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/defensa.jpg" height="30px" widht="30px" /></div>
        <p>Hazte con cientos de objetos y agregale tu toque personal a cada uno de ellos.<br />
        Con el sistema de customizacion de armas y armaduras podras modificarlos y agregarle atributos especiales para convertir ese objeto en uno mejor.</p>
    </li>

    <li>
        <h2>Misiones aqui, misiones haya</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/a0.jpg" height="30px" widht="30px" /></div>
        <p>Realiza divertidas misiones y obten recompensas unicas. Esto no es solo explorar novato, vamos, ¡muevete que el tiempo es oro y no tengo ninguno de los dos :P!.</p>
    </li>

    <li>
        <h2>¡Y mucho mas!</h2>
        <div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/2.jpg" height="30px" widht="30px" /></div>
        <p><b>Mercaderes</b>, <b>diferentes razas para seleccionar</b>, <b>subastas de objetos</b>, <b>comercio entre jugadores</b> son algunas de las otras caracteristicas que te encontraras en AresLands, ¿que esperas para jugar completamente gratis?, ¡te estamos esperando!.
    </li>
</ul>

<h2>¿Listo para jugar?</h2>
<div class="left" style="padding: 5px; padding-top: 0px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ac/a8.jpg" height="30px" widht="30px" /></div>
<p>Elije cual raza usaras para convertirte en el mejor en AresLands. Recuerda que cada clase tiene su especializacion, asi que, ¡escoge con cuidado y sabiamente!.</p>

<div class="right"><?php echo CHtml::link('<img src="' . Yii::app()->request->baseUrl . '/images/register-button.jpg" />', array('site/selectRace')); ?></div>