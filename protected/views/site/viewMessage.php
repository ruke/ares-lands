<?php if (Yii::app()->user->hasFlash('messageError')) { ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('messageError'); ?>
            </div>
<?php } else { ?>
            <?php 
            $sender = Characters::model()->findByPk($message->idFrom); 
            $message->read = true;
            $message->save();
            ?>

            <?php if (is_object($sender)) { ?>
                    <div class="dark-content"><b>From:</b> <?php echo CHtml::encode($sender->username); ?></div>
                    <div class="dark-content"><b>Subject:</b> <?php echo CHtml::encode($message->messageSubject); ?></div>
                    <div class="dark-content"><b>Message:</b>
                    <?php
                    $content = new BBCodeParser($message->messageText);
                    ?>
                    <div><?php echo nl2br($content->parseBbcode()->getText()); ?></div></div>
            <?php } else { ?>
                    <div class="flash-error">
                        <?php echo 'The sender character does not exist.'; ?>
                    </div>
            <?php } ?>
<?php } ?>