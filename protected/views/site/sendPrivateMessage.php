<h2><?php echo Yii::t('app', 'Send a private message'); ?></h2>

<?php if (Yii::app()->user->hasFlash('privateMessage')) { ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('privateMessage'); ?>
            </div>
<?php } elseif (Yii::app()->user->hasFlash('privateMessageError')) { ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('privateMessageError'); ?>
            </div>
<?php } else { ?>
            <div class="form">

                <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id'                        => 'private-message-form',
                        'enableClientValidation'    => true,
                        'clientOptions'             => array('validateOnSubmit' => true),
                        'htmlOptions'               => array('autocomplete' => 'off'),
                    ));
                ?>

                    <div class="row">
                        <?php
                            echo $form->labelEx($messageModel, 'idTo');
                            echo $form->dropDownList($messageModel, 'idTo', 
                                CHtml::listData($characters, 'id', 'username')
                            );
                            echo $form->error($messageModel, 'idTo');
                        ?>
                    </div>

                    <div class="row">
                        <?php
                            echo $form->labelEx($messageModel, 'messageSubject');
                            echo $form->textField($messageModel, 'messageSubject');
                            echo $form->error($messageModel, 'messageSubject');
                        ?>
                    </div>

                    <div class="row">
                        <?php
                            echo $form->labelEx($messageModel, 'messageText');
                            echo $form->textArea($messageModel, 'messageText');
                            echo $form->error($messageModel, 'messageText');
                        ?>
                    </div>

                    <div class="row buttons">
                        <?php echo CHtml::submitButton(Yii::t('app', 'Send private message')); ?>
                    </div>

                <?php $this->endWidget(); ?>
            </div>
<?php } ?>