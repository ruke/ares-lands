<?php
if (count($this->timers) > 0) {
    $this->renderPartial('_timers');
}

if (count($this->notifications) > 0) {
    $this->renderPartial('_notifications');
}

$gold = substr($user->gold, 0, -4) ? substr($user->gold, 0, -4) : 0;
$silver = substr($user->gold, -4, -2) ? substr($user->gold, -4, -2) : 0;
$copper = substr($user->gold, -2) ? substr($user->gold, -2) : 0;
?>

<div class="dark-content">
    <div class="right">
        <div class="right">
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m3.gif" /> <?php echo $gold; ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m2.gif" /> <?php echo $silver; ?>
            <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/m1.gif" /> <?php echo $copper; ?>
        </div>

        <div class="clear"></div>

        <div class="right" style="margin-top: 10px;">
            <?php
            $effects = Effects::model()->findAllByAttributes(array('idCharacter' => $user->id));

            foreach ($effects as $effect) {
                $effectItem = Items::model()->findByPk($effect->idItem);
            ?>
                <img tooltip="<?php echo '<b>' . $effectItem->name . '</b> (' . $effect->amount . ')'; ?> <div name='timer-0' style='text-align: right;'><?php echo Yii::t('app', 'Left time...'); ?></div>" src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $effect->idItem; ?>.jpg" width="30" height="30" />
            <?php
            }
            ?>
        </div>
    </div>

    <div class="left" style="padding-right: 5px;"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/ico<?php echo $user->idRace . $user->gender ?>.jpg" /></div>
    <h2><?php echo CHtml::encode($user->username); ?> - <i>"<?php echo CHtml::encode($user->title); ?>"</i></h2>
    
    <?php
    if ((int) $user->idGroup === 0) {
        echo Yii::t('app', 'You are not member of any clan, join one!');
    } else {
        $clan = Clans::model()->findByPk($user->idGroup);
        $secureClanName = CHtml::encode($clan->name);
        echo Yii::t('app', 'You are member of {name} clan', array('{name}' => CHtml::link($secureClanName, array('site/clan', 'name' => $secureClanName))));
    }
    ?>
</div>

<div class="left" style="border-right: 2px solid #500000; padding-right: 10px;">
    <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/<?php echo $user->idRace; ?>_<?php echo $user->gender; ?>_<?php echo $user->idWeapon; ?>.jpg" style="margin-top: 20px;" /></center>

    <div class="clear"></div>

    <div class="left" style="margin-top: 20px;">
        <h5><?php echo Yii::t('app', 'Stats'); ?></h5>
        <b><?php echo Yii::t('app', 'Damage'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->damage; ?>px;"><?php echo $user->damage; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Defense'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->defense; ?>px;"><?php echo $user->defense; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Armor penetration'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->armorPenetration; ?>px;"><?php echo $user->armorPenetration; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Evasion'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->evasion; ?>px;"><?php echo $user->evasion; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Luck'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->luck; ?>px;"><?php echo $user->luck; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Life'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->life; ?>px;"><?php echo $user->life; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Constitution'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->constitution; ?>px;"><?php echo $user->constitution; ?></div>
        </div> 

        <b><?php echo Yii::t('app', 'Accuracy'); ?></b>
        <div class="bar-background">
            <div class="bar-fill" style="width: <?php echo $user->accuracy; ?>px;"><?php echo $user->accuracy; ?></div>
        </div> 
    </div>

    <div class="clear"></div>

    <div class="left" style="margin-top: 20px;">
        <h5><?php echo Yii::t('app', 'Inventory'); ?></h5>
        <?php
        //$items = array();
        if (count($itemsInventory) > 0) {
            /*foreach ($itemsInventory as $item) {
                $item = Items::model()->findByPk($item->idItem);

                if (isset($items[$item->name]['dropDownList'])) {
                    $items[$item->name]['dropDownList'][count($items[$item->name]['dropDownList']) + 1] = count($items[$item->name]['dropDownList']) + 1;
                } else {
                    if ((int) $item->type === Items::POTION_TYPE) {
                        $items[$item->name] = array();
                        $items[$item->name]['data'] = $item;
                        $items[$item->name]['dropDownList'] = array();
                        $items[$item->name]['dropDownList'][1] = 1;
                    } else {
                        $items[] = array('data' => $item);
                    }
                }
            }

            foreach ($items as $item) {*/
            foreach ($itemsInventory as $item) {
                $itemInfo = Items::model()->findByPk($item->idItem);
                $item->custom = unserialize($item->custom);
        ?>
                <div class="dark-content" style="width: 220px;">
                    <?php 
                    //echo CHtml::beginForm(array('site/dropItem'), 'post'); 
                    echo CHtml::beginForm();
                    //echo CHtml::hiddenField('idItem', $item['data']->id);
                    echo CHtml::hiddenField('idItem', $itemInfo->id);
                    ?>
                    <div name="menu" tooltip="clic para mas info">
                        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $itemInfo->id; ?>.jpg" width="30px" height="30px" style="vertical-align: middle;" /> <?php echo $itemInfo->name; ?>
                        <div class="right">
                            <?php 
                            if ((int) $itemInfo->type === Items::POTION_TYPE) {
                                $amount = array();

                                //for($i = 1, $max = $item->custom['amount']; $i <= $max; ++$i) {
                                for ($i = 1, $max = $item->amount; $i <= $max; ++$i) {
                                    $amount[$i] = $i;
                                }

                                echo CHtml::dropDownList('amount', 'amount', $amount);
                            } 
                            ?>
                        </div>
                        <div name="menuInnerContent">
                            <ul style="list-style: none; margin: 0px; margin-top: 5px; padding: 0px;">
                                <?php
                                switch ((int) $itemInfo->type) {
                                    case Items::WEAPON_TYPE:
                                        echo '<li>' . CHtml::link('Equip weapon', array('site/equipWeapon', 'idWeapon' => $itemInfo->id)) . '</li>';
                                        break;

                                    case Items::SHIELD_TYPE:
                                        echo '<li>' . CHtml::link('Equip shield', array('site/equipShield', 'idShield' => $itemInfo->id)) . '</li>';
                                        break;

                                    case Items::POTION_TYPE:
                                        echo '<li>' . CHtml::linkButton(Yii::t('app', 'Use consumable'), array(
                                            'submit'    => array('site/useConsumable'),
                                        )) . '</li>';
                                        break;
                                }
                                echo '<li>' . CHtml::linkButton(Yii::t('app', 'Drop item'), array(
                                    'submit'    => array('site/dropItem'),
                                )) . '</li>';
                                ?>
                            </ul>
                        </div>
                    </div>
                    <?php echo CHtml::endForm(); ?>
                </div>
        <?php
            }
        } else {
            echo Yii::t('app', 'You do not have objects in your inventory');
        }
        ?>
    </div>
</div>

<div class="left" style="padding-left: 16px;">
    <div style="margin-top: 20px;">
        <h5><?php echo Yii::t('app', 'Weapon');
        if ($user->idWeapon > 0) {
            $weapon = Items::model()->findByPk($user->idWeapon);
            echo ': ' . $weapon->name . ' ';
            echo CHtml::link(Yii::t('app', '(Save weapon in inventory)'), array('site/saveWeaponInInventory'));
            echo '</h5>';
        ?>
            <div class="clear"></div>

            <div class="left" style="padding-right: 10px; width: 190px;">      
                <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $user->idWeapon; ?>.jpg" /></center>
            </div>

            <div class="right" style="width: 440px;">
                <?php if ($weapon->damage != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Attack'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->damage; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->defense != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Defense'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->defense; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->armorPenetration != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Armor penetration'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->armorPenetration; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->evasion != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Evasion'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->evasion; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->luck != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Luck'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->luck; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->life != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Life'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->life; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->constitution != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Constitution'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->constitution; ?></div>
                    </div>
                <?php } ?>

                <?php if ($weapon->accuracy != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Accuracy'); ?>:</b></div>
                        <div class="right"><?php echo $weapon->accuracy; ?></div>
                    </div>
                <?php } ?>
            </div>
        <?php
        } else {
        ?>
            </h5>
            <div class="dark-content">
                <?php echo Yii::t('app', 'You do not have weapon'); ?>
            </div>
        <?php
        }
        ?>
    </div>

    <div class="clear"></div>

    <div style="margin-top: 20px;">
        <h5><?php echo Yii::t('app', 'Shield');
        if ($user->idShield > 0) {
            $shield = Items::model()->findByPk($user->idShield);
            echo ': ' . $shield->name . ' ';
            echo CHtml::link(Yii::t('app', '(Save shield in inventory)'), array('site/saveShieldInInventory'));
            echo '</h5>';
        ?>  
            <div class="left" style="padding-right: 10px; width: 190px;">      
                <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $user->idShield; ?>.jpg" /></center>
            </div>

            <div class="right" style="width: 440px;">
                <?php if ($shield->damage != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Attack'); ?>:</b></div>
                        <div class="right"><?php echo $shield->damage; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->defense != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Defense'); ?>:</b></div>
                        <div class="right"><?php echo $shield->defense; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->armorPenetration != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Armor penetration'); ?>:</b></div>
                        <div class="right"><?php echo $shield->armorPenetration; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->evasion != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Evasion'); ?>:</b></div>
                        <div class="right"><?php echo $shield->evasion; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->luck != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Luck'); ?>:</b></div>
                        <div class="right"><?php echo $shield->luck; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->life != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Life'); ?>:</b></div>
                        <div class="right"><?php echo $shield->life; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->constitution != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Constitution'); ?>:</b></div>
                        <div class="right"><?php echo $shield->constitution; ?></div>
                    </div>
                <?php } ?>

                <?php if ($shield->accuracy != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Accuracy'); ?>:</b></div>
                        <div class="right"><?php echo $shield->accuracy; ?></div>
                    </div>
                <?php } ?>
            </div>
        <?php
        } else {
        ?>
            </h5>
            <div class="dark-content">
                <?php echo Yii::t('app', 'You do not have shield'); ?>
            </div>
        <?php
        }
        ?>
    </div>

    <div class="clear"></div>

    <div style="margin-top: 20px;">
        <h5><?php echo Yii::t('app', 'Companion');
        if ($user->idCompanion > 0) {
            $companion = Items::model()->findByPk($user->idCompanion);
            echo ': ' . $companion->name . '</h5>';
        ?>  
            <div class="left" style="padding-right: 10px; width: 190px;">      
                <center><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/armas/<?php echo $user->idCompanion; ?>.jpg" /></center>
            </div>

            <div class="right" style="width: 440px;">
                <?php if ($companion->damage != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Attack'); ?>:</b></div>
                        <div class="right"><?php echo $companion->damage; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->defense != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Defense'); ?>:</b></div>
                        <div class="right"><?php echo $companion->defense; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->armorPenetration != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Armor penetration'); ?>:</b></div>
                        <div class="right"><?php echo $companion->armorPenetration; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->evasion != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Evasion'); ?>:</b></div>
                        <div class="right"><?php echo $companion->evasion; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->luck != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Luck'); ?>:</b></div>
                        <div class="right"><?php echo $companion->luck; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->life != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Life'); ?>:</b></div>
                        <div class="right"><?php echo $companion->life; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->constitution != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Constitution'); ?>:</b></div>
                        <div class="right"><?php echo $companion->constitution; ?></div>
                    </div>
                <?php } ?>

                <?php if ($companion->accuracy != 0) { ?>
                    <div class="dark-content" style="height: 10px;">
                        <div class="left"><b><?php echo Yii::t('app', 'Accuracy'); ?>:</b></div>
                        <div class="right"><?php echo $companion->accuracy; ?></div>
                    </div>
                <?php } ?>
            </div>
        <?php
        } else {
        ?>
            <?php echo Yii::t('app', 'You do not have companion'); ?>
        <?php
        }
        ?>
    </div>
</div>

<script asyc type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tooltip.js"></script>
<script asyc type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/menu.js"></script>