<div class="dark-content" style="margin-top: 10px; margin-bottom: 10px;">
    <h5><?php echo Yii::t('app', 'Notifications'); ?></h5>
    <?php foreach ($this->notifications as $notification) { ?>
        <div class="light-content">
            <?php if ($notification->icon) { ?>
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/<?php echo $notification->icon; ?>" style="vertical-align: middle; margin-right: 10px;" width="30" height="30" />
            <?php } ?>
            <?php echo $notification->content; ?>
        </div>
    <?php } ?>
</div>