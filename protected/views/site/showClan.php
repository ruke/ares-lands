<?php
echo '<center><h2>' . $clan->name . '</h2></center>';
$content = new BBCodeParser($clan->description);
echo $content->parseBbcode()->getText() . '</p>';
?>

<?php
if ((int) $character->id === (int) $clan->idCreator) {
    ?>
    <h5>Petitions</h5>
    <?php
    $petitions = ClanPetitions::model()->findAllByAttributes(array('idGroup' => $clan->id));

    if (count($petitions) > 0) {
        foreach ($petitions as $petition) {
            $petitionCharacter = Characters::model()->findByPk($petition->idCharacter);
    ?>
            <div class="dark-content">
                <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/ico<?php echo $petitionCharacter->idRace . $petitionCharacter->gender; ?>.jpg" style="vertical-align: middle;" />
                <b style="margin-left: 5px;"><?php echo $petitionCharacter->username; ?></b>
                (Level: <?php echo $petitionCharacter->level; ?>)

                <div class="right">
                    <span tooltip="accept" style="padding: 5px;"><?php echo CHtml::link('v', array('site/acceptCharacterInClan', 'idCharacter' => $petitionCharacter->id)); ?></span>
                    <span tooltip="reject" style="padding: 5px;"><?php echo CHtml::link('x', array('site/rejectCharacterInClan', 'idCharacter' => $petitionCharacter->id)); ?></span>
                </div>
            </div>
    <?php
        }
    } else {
        echo 'There are no petitions';
    }
    ?>
<?php
}
?>

<h5>Members</h5>
<?php
foreach ($members as $member) {
?>
    <div class="dark-content">
        <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/prota/ico<?php echo $member->idRace . $member->gender; ?>.jpg" style="vertical-align: middle;" />
        <b style="margin-left: 5px;"><?php echo $member->username; ?></b>
        (Level: <?php echo $member->level; ?>)

        <?php if ((int) $clan->idCreator === (int) $member->id) { ?>
            <div class="right">Creator</div>
        <?php } ?>

        <?php if ((int) $character->id === (int) $clan->idCreator && (int) $character->id !== (int) $member->id) { ?>
            <div class="right">
                <span tooltip="remove member from clan"><?php echo CHtml::link('x', array('site/removeMemberFromClan', 'idCharacter' => $member->id)); ?></span>
            </div>
        <?php } ?>
    </div>
<?php
}

if ((int) $character->idGroup === 0) {
    echo CHtml::link('Send petition to join', array('site/sendPetitionToJoinClan', 'id' => $clan->id));
} else {
    if ((int) $character->idGroup === (int) $clan->id && (int) $character->id !== (int) $clan->idCreator) {
        echo CHtml::link('Leave from the clan', array('site/leaveFromClan'));
    }
}
?>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/tooltip.js"></script>