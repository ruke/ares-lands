<?php
$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
if ((int) $character->idGroup === 0) {
    echo CHtml::link('create one', array('site/createClan'));
}

if (count($clans) > 0) {
    foreach ($clans as $clan) {
        $name = CHtml::encode($clan->name);
        echo '<p>' . CHtml::link($name, array('site/clan', 'name' => $name)) . '<br />';
        $content = new BBCodeParser($clan->description);
        //echo $content->cleanBbcode()->getText() . '</p>';
        echo $content->parseBbcode()->getText() . '</p>';
    }
} else {
    echo 'There are no clans, maybe you can create one';
}
?>