<h2>Trade</h2>
<?php if (Yii::app()->user->hasFlash('trade')) { ?>
            <div class="flash-success">
                <?php echo Yii::app()->user->getFlash('trade'); ?>
            </div>
<?php } elseif (Yii::app()->user->hasFlash('tradeError')) { ?>
            <div class="flash-error">
                <?php echo Yii::app()->user->getFlash('tradeError'); ?>
            </div>
<?php } else { ?>
            <div class="form">

                <?php
                    $form = $this->beginWidget('CActiveForm', array(
                        'id'                        => 'trade-form',
                        'enableClientValidation'    => true,
                        'clientOptions'             => array('validateOnSubmit' => true),
                        'htmlOptions'               => array('autocomplete' => 'off'),
                    ));
                ?>

                    <div class="row">
                        <?php
                            echo $form->labelEx($tradeForm, 'idBuyer');
                            echo $form->dropDownList($tradeForm, 'idBuyer', 
                                CHtml::listData($characters, 'id', 'username')
                            );
                            echo $form->error($tradeForm, 'idBuyer');
                        ?>
                    </div>

                    <div class="row">
                        <?php
                            $itemInfo = array();
                            $item;
                            foreach ($inventoryItems as $inventoryItem) {
                                $item = Items::model()->findByPk($inventoryItem->idItem);
                                $itemInfo[$item->id] = $item->name;
                            }

                            echo $form->labelEx($tradeForm, 'idItem');
                            echo $form->dropDownList($tradeForm, 'idItem', $itemInfo, array(
                                'empty' => Yii::t('app', 'Select item'),
                                'ajax'  => array(
                                    'type'  => 'POST',
                                    'url'   => array('site/GetItemAmount'),
                                    'update'=> '#' . CHtml::activeId($tradeForm, 'amount'),
                                ),
                            ));
                            echo $form->error($tradeForm, 'idItem');
                        ?>
                    </div>

                    <div class="row">
                        <?php
                            echo $form->labelEx($tradeForm, 'amount');
                            echo $form->dropDownList($tradeForm, 'amount', array(0));
                            echo $form->error($tradeForm, 'amount');
                        ?>
                    </div>

                    <div class="row">
                        <?php
                            echo $form->labelEx($tradeForm, 'price');
                            echo $form->textField($tradeForm, 'price');
                            echo $form->error($tradeForm, 'price');
                        ?>
                    </div>

                    <div class="row buttons">
                        <?php echo CHtml::submitButton(Yii::t('app', 'Trade')); ?>
                    </div>

                <?php $this->endWidget(); ?>

            </div>
<?php } ?>