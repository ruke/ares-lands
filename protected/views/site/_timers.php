<div id="timers" class="dark-content" style="margin-top: 10px; margin-bottom: 10px;"><h5><?php echo Yii::t('app', 'Timers'); ?></h5></div>

<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/timer.js"></script>

<script type="text/javascript">
    var time;
    var div;
    var parent = $('#timers');
    var count = 0;

    <?php 
    $now = time();
    $hours;
    $minutes;
    $seconds;
    foreach ($this->timers as $timer) { 
    ?>
        <?php
        $falta = $timer->ends - $now;
        $hours = (int) ($falta / 3600);
        $minutes = (int) ($falta / 60 - $hours * 60);
        $seconds = (int) ($falta - $hours * 3600 - $minutes * 60);
        ?>
        div = $('<div class="light-content"></div>');
        parent.append(div);
        div.append('<span><?php echo $timer->description . ": "; ?></span>');
        div.append('<span name="timer-' + count + '"></span>');
        newTimer('timer-' + count, <?php echo $hours . ',' . $minutes . ',' . $seconds; ?>);
        ++count;
    <?php } ?>
</script>