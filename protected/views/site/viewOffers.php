<h2>View offers</h2>

<div class="form dark-content">

    <?php foreach ($offers as $offer) { ?>
        <?php $itemInfo = Items::model()->findByPk($offer->idItem); ?>

        <?php echo CHtml::beginForm(); ?>
            <?php echo CHtml::hiddenField('idBatch', $offer->id); ?>
            <?php echo $itemInfo->name; ?>
            <?php echo CHtml::textField('price'); ?>
            <?php echo CHtml::linkButton('Offer for this item', array(
                'submit'    => array('site/makeOffer'),
            )); ?>
        <?php echo CHtml::endForm(); ?>
    <?php } ?>

</div>