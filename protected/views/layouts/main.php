<!DOCTYPE HTML>
<html lang="es">
<head>
    <meta charset="UTF-8">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/normalize.css" />

    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
    <![endif]-->

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
</head>

<body>

<div class="container" id="page">

    <div id="header">
        <div id="logo"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo.jpg" /></div>
    </div><!-- header -->

    <div class="separator-bar-alternative"></div>
    <div id="mainmenu">
        <?php 
        $items = array();
        $items[] = array('label'=>'Inicio', 'url'=>array('/site/index'));
        $items[] = array('label'=>'Registrate para jugar', 'url'=>array('/site/selectRace'), 'visible'=>Yii::app()->user->isGuest);
        $items[] = array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest);
        $items[] = array('label'=>'Clan', 'url' => array('site/showClans'), 'visible' => !Yii::app()->user->isGuest);

        if (!Yii::app()->user->isGuest) {
            $messages = Messages::model()->findAllByAttributes(array(
                'idTo'  => Yii::app()->user->character->id,
                'read'  => false,
            ));

            $messagesLabel = 'Messages';
            
            if (count($messages) > 0) {
                $messagesLabel .= ' (' . count($messages) . ')';
            }

            $items[] = array('label'=>$messagesLabel, 'url'=>array('site/messages'));
            $items[] = array('label'=>'Trade', 'url'=>array('site/trade'));
            $items[] = array('label'=>'Logout ('.Yii::app()->user->character->username.')', 'url'=>array('/site/logout'));
        }

        $this->widget('zii.widgets.CMenu',array(
            'items'=> $items
        )); 

        unset($items);
        ?>

        <div class="wood-separator-bar" style="height: 10px;"></div>

        <?php
        if (count($this->npcs) > 0) {
            foreach ($this->npcs as $npc) {
        ?>
                <div style="display: inline-block;">
                    <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/humano_ico.jpg" />
                    <br />
                    <?php echo CHtml::link($npc->name, array('site/npc', 'name' => $npc->name)); ?>
                </div>
        <?php
            }
        }
        ?>
        <div class="clear"></div>
    </div><!-- mainmenu -->
    <div class="separator-bar-alternative"></div>

    <?php if(isset($this->breadcrumbs)):?>
        <?php $this->widget('zii.widgets.CBreadcrumbs', array(
            'links'=>$this->breadcrumbs,
        )); ?><!-- breadcrumbs -->
    <?php endif?>

    <?php echo $content; ?>

    <div class="clear"></div>

    <div id="footer">
        Idea original de <b>V. Buendia</b> ('<a href="http://sourceforge.net/projects/tierras/">Tierras de Leyenda</a>')<br />
        <p>Iron Fist</p>
        <?php echo Yii::powered(); ?>
    </div><!-- footer -->

</div><!-- page -->

</body>
</html>