<?php

class SiteController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha'=>array(
                'class'=>'CCaptchaAction',
                'backColor'=>0x600000,
                'foreColor'=>0xF9DD1C,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page'=>array(
                'class'=>'CViewAction',
            ),
        );
    }
    
    public function actionIndex()
    {
        if (Yii::app()->user->isGuest) {
            $this->render('index');
        } else {
            //$user = Characters::model()->findByPk((int) Yii::app()->user->getId());
            $user = Yii::app()->user->character;
            $itemsInventory = Inventory::model()->findAllByAttributes(array('idCharacter' => (int) $user->id));
            
            $this->render('general', array('user' => $user, 'itemsInventory' => $itemsInventory));
        }
    }

    public function actionSelectRace()
    {
        $this->render('selectRace');
    }

    public function actionRegister($raceId)
    {
        $raceId = (int) $raceId;

        if (!$raceId || $raceId > 4 || $raceId < 1) {
            $this->redirect(array('site/selectRace'));
        }

        $registerForm = new RegisterForm();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'register-form') {
            echo CActiveForm::validate($registerForm);
        } else {
            if (isset($_POST['RegisterForm'])) {
                $registerForm->attributes = $_POST['RegisterForm'];
                $registerForm->idRace = $raceId;
                $registerForm->level = 1;
                $registerForm->gold = 10;
                $registerForm->idZone = 1;
                
                if ($registerForm->save()) {
                    Yii::app()->user->setFlash('register', Yii::t('app', 'You have registered successfully'));
                }
            }
        }

        $this->render('register', array(
            'registerForm' => $registerForm,
        ));
    }

    // AJAX
    // Devolvemos la cantidad de items que posee
    public function actionGetItemAmount()
    {
        if (!isset($_POST['TradeForm']) && !isset($_POST['TradeForm']['idItem'])) {
            $this->redirect(array('site/index'));
        }

        $data = Inventory::model()->findByAttributes(array(
            'idCharacter'   => Yii::app()->user->character->id,
            'idItem'        => (int) $_POST['TradeForm']['idItem'],
        ));

        if (!is_object($data)) {
            echo CHtml::tag('option', array('value' => 0), 0, true);
            return;
        }

        /*if ($data->custom) {
            $data->custom = unserialize($data->custom);
            $amount = $data->custom['amount'];
        } else {
            $amount = 1;
        }*/

        for ($i = 1; $i <= $data->amount; ++$i) {
            echo CHtml::tag('option', array('value' => $i), $i, true);
        }
    }

    public function actionSendPrivateMessage()
    {
        $messageModel = new Messages();
        $characters = Characters::model()->findAll();

        if (isset($_POST['ajax']) && $_POST['ajax'] === 'private-message-form') {
            echo CActiveForm::validate($messageModel);
        } else {
            if (isset($_POST['Messages'])) {
                $messageModel->attributes = $_POST['Messages'];
                $messageModel->idFrom = Yii::app()->user->character->id;
                
                if ($messageModel->save()) {
                    Yii::app()->user->setFlash('privateMessage', Yii::t('app', 'Your private message was sent'));
                } else {
                    Yii::app()->user->setFlash('privateMessageError', Yii::t('app', 'Your private message was not sent, please try again'));
                }
            }
        }

        $this->render('sendPrivateMessage', array(
            'messageModel'  => $messageModel,
            'characters'    => $characters,
        ));
    }

    public function actionDeleteMessage($id)
    {
        $message = Messages::model()->findByPk((int) $id);

        if (is_object($message)) {
            $message->delete();
        }

        $this->redirect(array('site/messages'));
    }

    public function actionMessages($id = null)
    {
        if ($id === null) {
            $messages = Messages::model()->findAllByAttributes(array(
                'idTo'  => Yii::app()->user->character->id,
            ));

            $this->render('messages', array('messages' => $messages));
        } else {
            $message = Messages::model()->findByPk((int) $id);

            if (!is_object($message) || (int) $message->idTo !== (int) Yii::app()->user->character->id) {
                Yii::app()->user->setFlash('messageError', Yii::t('app', 'Error on displaying the message'));
            }

            $this->render('viewMessage', array('message' => $message));
        }
    }
    public function actionDeclineTrade ($id)
    {
        $id = (int) $id;
        $trade = Trades::model()->findByPk($id);

        if (is_object($trade)) {
            $seller = Characters::model()->findByPk((int) $trade->idSeller);

            if (is_object($seller)) {
                $inventario = Inventory::model()->findByAttributes(array(
                    'idCharacter' => $seller->id,
                    'idItem' => $trade->idItem
                ));

                if(is_object($inventario)) {
                    $inventario->amount = $inventario->amount + $trade->amount;
                    $inventario->save();
                } else {
                    $inventario = new Inventory();

                    $inventario->idCharacter = $seller->id;
                    $inventario->idItem = $trade->idItem;
                    $inventario->custom = $trade->custom;

                    $inventario->save();
                    //que hay que enviar el mp a los dos (tanto comprador como vendedor) para notificación
                }
            }
        }
    }
    public function actionAcceptTrade($id)
    {
        $id = (int) $id;
        $trade = Trades::model()->findByPk((int) $id);

        if (is_object($trade)) {
            $buyer = Characters::model()->findByPk((int) Yii::app()->user->character->id);

            if ((int) $trade->idBuyer === (int) $buyer->id) {
                if ($trade->price <= $buyer->gold) {
                    $seller = Characters::model()->findByPk((int) $trade->idSeller);
                    
                    if (is_object($seller)) {
                        $inventoryBuyer = new Inventory();

                        $inventoryBuyer->idCharacter = $buyer->id;
                        $inventoryBuyer->idItem = $trade->idItem;
                        $inventoryBuyer->amount = $trade->amount;
                        $inventoryBuyer->custom = $trade->custom;

                        $inventoryBuyer->save();
                        
                        $buyer->gold = $buyer->gold - $trade->price;
                        $buyer->save();

                        $seller->gold = $seller->gold + $trade->price;
                        $seller->save();

                        $trade->delete();

                        Yii::app()->user->setFlash('trade', Yii::t('app', 'The trade was successfully done'));
                    } else {
                        Yii::app()->user->setFlash('tradeError', Yii::t('app', 'You can not accept the trade because the seller does not exist'));
                    }
                } else {
                    Yii::app()->user->setFlash('tradeError', Yii::t('app', 'You can not accept the trade because you do not have enough money'));
                }
            } else {
                Yii::app()->user->setFlash('tradeError', Yii::t('app', 'That trade does not includes you')); 
            }
        } else {
            Yii::app()->user->setFlash('tradeError', Yii::t('app', 'That trade does not exist'));
        }

        $this->render('acceptTrade');
    }

    public function actionTrade()
    {
        $tradeForm = new TradeForm();
        $characters = Characters::model()->findAll('id <> :id', array(':id' => Yii::app()->user->character->id));
        $inventoryItems = Inventory::model()->findAllByAttributes(array('idCharacter' => (int) Yii::app()->user->character->id));
        $inventoryItem;
        
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'trade-form') {
            echo CActiveForm::validate($tradeForm);
        } else {
            if (isset($_POST['TradeForm'])) {
                $tradeForm->attributes = $_POST['TradeForm'];
                $tradeForm->idSeller = Yii::app()->user->character->id;

                $inventoryItem = Inventory::model()->findByAttributes(array('idItem' => $tradeForm->idItem, 'idCharacter' => Yii::app()->user->character->id));

                if($tradeForm->amount < $inventoryItem->amount){
                    Yii::app()->user->setFlash('tradeError', Yii::t('app', 'You do not have this item stock'));
                } else {
                    if($tradeForm->amount === $inventoryItem->amount){
                        $inventoryItem->delete();
                    } else {
                        $inventoryItem->amount = $inventoryItem->amount - $tradeForm->amount;
                        $inventoryItem->save();
                    }

                    $tradeForm->save();
                    
                    $item = Items::model()->findByPk($tradeForm->idItem); // check here

                    $message = new Messages();

                    $message->idFrom = Yii::app()->user->character->id;
                    $message->idTo = $tradeForm->idBuyer;
                    $message->messageSubject = 'Trade!';
                    $message->messageText = Yii::app()->user->character->username . ' wants to trade with you.
                    Here is the information of the operation.

                    [img]' . Yii::app()->request->baseUrl . '/images/armas/' . $tradeForm->idItem . '.jpg[/img]
                    [b]Item name:[/b] ' . $item->name . '
                    [b]Amount:[/b] ' . (int) $tradeForm->amount . '
                    [b]Price:[/b] ' . (int) $tradeForm->price . '

                    [url=' . $this->createUrl('site/acceptTrade', array('id' => $tradeForm->id)) . ']Accept offer[/url] or reject it';
                    $message->date = time();

                    $message->save();

                    Yii::app()->user->setFlash('trade', Yii::t('app', 'You have been started the trade!. Now just wait for answer'));
                } 
            }
        }

        $this->render('trade', array(
            'tradeForm'         => $tradeForm,
            'characters'        => $characters,
            'inventoryItems'    => $inventoryItems,
        ));
    }
    /***********************************************REVISAR*****************************************************/
    public function actionViewOffers (){
        $offers = Auctions::model()->findAll();
        $this->render('viewOffers', array('offers' => $offers));
    }

    public function actionMakeOffer (){
        $idLote = (int) $_POST['idBatch'];
        $offerBuyer = (int) $_POST['price'];
        $character = Yii::app()->user->character;
        $auction = Auctions::model()->findByPk($idLote);
        if (is_object ($auction)){
            if($auction->offer > 0){
                $offerMin = $auction->price * 0.10 + $auction->offer;
                $characterLoser = Characters::model()->findByPk((int) $auction->idBuyer);
                
                /*ENVIAR MENSAJE DE NOTIFICACION*/
            } else {
                $offerMin = $auction->price;
            }
            if($character->gold >= $offerBuyer){
                if ($offerBuyer >= $offerMin){
                    $auction->offer = $offerBuyer;
                    $auction->idBuyer = $character->id;
                    $character->gold = $character->gold - $offerBuyer;
                    
                    $character->save();
                    $auction->save();
                    if(is_object($characterLoser)){
                        $characterLoser->gold = $characterLoser->gold + $auction->offer;
                        $characterLoser->save();
                    }
                } else {
                    Yii::app()->user->setFlash('auctionError', Yii::t('app', 'Your offer is insuficient'));
                }
            } else {
                Yii::app()->user->setFlash('auctionError', Yii::t('app', 'You do not have gold'));
            }
        } else {
            Yii::app()->user->setFlash('auctionError', Yii::t('app', 'That batch does not exist'));
        }
    }

    public function actionAuction(){
        $auction = new Auctions();
        $inventoryItem = Inventory::model()->findByAttributes(array('idItem' => $auction->idItem, 'idCharacter' => Yii::app()->user->character->id));
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'auction-form') {
            echo CActiveForm::validate($auction);
        } else {
            if (isset($_POST['Auctions'])) {
                $auction->attributes = $_POST['Auctions'];
                if (is_object($inventoryItem)) {
                    $item = Items::model()->findByPk($auction->idItem);
                    if (is_object($item)) { // verificamos que el objeto exista
                        if ((int) $item->type === Items::POTION_TYPE) {
                            if($auction->amount < $inventoryItem->amount){
                                Yii::app()->user->setFlash('auctionError', Yii::t('app', 'You do not have this item stock'));
                            } else {
                                if($auction->amount === $inventoryItem->amount){
                                    $inventoryItem->delete();
                                } else {
                                    $inventoryItem->amount = $inventoryItem->amount - $auction->amount;
                                    $inventoryItem->save();
                                }
                                $auction->save();
                            }
                        } else {
                            if($auction->amount > 1 ){
                                Yii::app()->user->setFlash('auctionError', Yii::t('app', 'This item is selling one for auction')); // WTF?!
                            } else {
                                $inventoryItem->delete();
                                $auction->save();  
                            }
                        }
                    } else 
                        Yii::app()->user->setFlash('auctionError', Yii::t('app', 'That item does not exist'));
                } else {
                    Yii::app()->user->setFlash('auctionError', Yii::t('app', 'You do not have this item'));
                }
                
            }
        }
        $this->render('auction', array(
            'auctionForm' => $auction, 
            'inventoryItems' => $inventoryItem
        ));
    }
    /*********************************************************************************************************/
    public function actionShowClans()
    {
        $clans = Clans::model()->findAll();
        $this->render('showClans', array('clans' => $clans));
    }

    public function actionClan($name)
    {
        $clan = Clans::model()->findByAttributes(array('name' => $name));
        $members = Characters::model()->findAllByAttributes(array('idGroup' => $clan->id));
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $this->render('showClan', array('clan' => $clan, 'members' => $members, 'character' => $character));
    }

    public function actionCreateClan()
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;

        if ((int) $character->idGroup === 0) {
            $clanForm = new Clans();

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'create-clan-form') {
                echo CActiveForm::validate($clanForm);
            } else {
                if (isset($_POST['Clans'])) {
                    $clanForm->attributes = $_POST['Clans'];
                    $clanForm->idCreator = $character->id;
                    
                    if ($clanForm->save()) {
                        $character->idGroup = $clanForm->id;
                        $character->save();

                        Yii::app()->user->setFlash('create', Yii::t('app', 'You have created successfully a clan named "{name}"'), array('{name}' => $clanForm->name));
                    }
                }
            }

            $this->render('createClan', array('clanForm' => $clanForm));
        } else {
            $this->redirect(array('site/clan', 'name' => Clans::model()->findByPk($character->idGroup)->name));
        }
    }

    public function actionLeaveFromClan()
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;

        if (is_object($character)) {
            $notification = new Notifications();
            $notification->idCharacter = $character->id;

            if ((int) $character->idGroup !== 0) {
                $clan = Clans::model()->findByPk($character->idGroup);

                if (is_object($clan)) {
                    if ((int) $character->id !== (int) $clan->idCreator) {
                        $members = Characters::model()->findAllByAttributes(array('idGroup' => $character->idGroup));

                        foreach ($members as $member) {
                            if ((int) $member->id === (int) $character->id) {
                                continue;
                            }

                            $notificationToMember = new Notifications();

                            $notificationToMember->idCharacter = $member->id;
                            $notificationToMember->content = Yii::t('app', 'Member "{name}" has left from the clan', array('{name}' => $character->username));

                            $notificationToMember->save();
                        }

                        $notification->content = Yii::t('app', 'You has left from the "{name}" clan', array('{name}' => $clan->name));

                        $character->idGroup = 0;
                        $character->save();
                    } else {
                        $notification->content = Yii::t('app', 'You can not leave from the clan if you are the leader');
                    }
                } else {
                    $notification->content = Yii::t('app', 'That clan does not exist');
                }
            } else {
                $notification->content = Yii::t('app', 'You are not member of any clan');
            }

            $notification->save();
        }

        $this->redirect(array('site/index'));
    }

    public function actionRemoveMemberFromClan($idCharacter)
    {
        $character = Characters::model()->findByPk((int) $idCharacter);
        //$characterLeader = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $characterLeader = Yii::app()->user->character;

        if (is_object($characterLeader)) {
            $notification = new Notifications();
            $notification->idCharacter = $characterLeader->id;

            if (is_object($character)) {
                $clan = Clans::model()->findByPk($characterLeader->idGroup);

                if (is_object($clan)) {
                    if ((int) $clan->idCreator === (int) $characterLeader->id) {
                        if ((int) $clan->id === (int) $character->idGroup) {
                            if ((int) $idCharacter !== (int) $characterLeader->id) {
                                $notification->content = Yii::t('app', 'The character "{name}" was removed from the clan', array('{name}' => $character->username));

                                $character->idGroup = 0;
                                $character->save();

                                $notificationToCharacter = new Notifications();

                                $notificationToCharacter->idCharacter = $character->id;
                                $notificationToCharacter->content = Yii::t('app', 'You have been removed from "{name}" clan', array('{name}' => $clan->name));

                                $notificationToCharacter->save();
                            } else {
                                $notification->content = Yii::t('app', 'Error: You can not remove yourself (leader) from the clan');
                            }
                        } else {
                            $notification->content = Yii::t('app', 'Error: That user does not belongs to the clan');
                        }
                    } else {
                        $notification->content = Yii::t('app', 'Error: You are not the clan leader!. Only clan leaders can reject petitions');
                    }
                } else {
                    $notification->content = Yii::t('app', 'Error: That clan does not exist');
                }
            } else {
                $notification->content = Yii::t('app', 'Error: That user does not exist');
            }

            $notification->save();
        }

        $this->redirect(array('site/index'));
    }

    public function actionRejectCharacterInClan($idCharacter)
    {
        $character = Characters::model()->findByPk((int) $idCharacter);
        //$characterLeader = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $characterLeader = Yii::app()->user->character;

        if (is_object($characterLeader)) {
            $notification = new Notifications();
            $notification->idCharacter = $characterLeader->id;

            if (is_object($character)) {
                $clan = Clans::model()->findByPk($characterLeader->idGroup);

                if (is_object($clan)) {
                    if ((int) $clan->idCreator === (int) $characterLeader->id) {
                        $notification->content = Yii::t('app', 'The petition made by the character "{name}" was rejected', array('{name}' => $character->username));

                        $petition = ClanPetitions::model()->findByAttributes(array('idCharacter' => $character->id, 'idGroup' => $clan->id));
                        $petition->delete();

                        $notificationToCharacter = new Notifications();

                        $notificationToCharacter->idCharacter = $character->id;
                        $notificationToCharacter->content = Yii::t('app', 'Your petition to join into "{name}" clan was rejected', array('{name}' => $clan->name));

                        $notificationToCharacter->save();
                    } else {
                        $notification->content = Yii::t('app', 'Error: You are not the clan leader!. Only clan leaders can reject petitions');
                    }
                } else {
                    $notification->content = 'Error: That clan does not exist';
                }
            } else {
                $notification->content = 'Error: That user does not exist';
            }

            $notification->save();
        }

        $this->redirect(array('site/index'));
    }

    public function actionAcceptCharacterInClan($idCharacter)
    {
        $character = Characters::model()->findByPk((int) $idCharacter);
        //$characterLeader = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $characterLeader = Yii::app()->user->character;

        if (is_object($characterLeader)) {
            $notification = new Notifications();
            $notification->idCharacter = $characterLeader->id;

            if (is_object($character) && (int) $character->idGroup === 0) {
                $clan = Clans::model()->findByPk($characterLeader->idGroup);

                if (is_object($clan)) {
                    if ((int) $clan->idCreator === (int) $characterLeader->id) {
                        $members = Characters::model()->findAllByAttributes(array('idGroup' => $characterLeader->idGroup));

                        if (count($members) < Clans::MEMBER_LIMIT) {
                            $notification->content = Yii::t('app', 'The character "{name}" was correctly accepted into the clan', array('{name}' => $character->username));

                            $clanPetitions = ClanPetitions::model()->findAllByAttributes(array('idCharacter' => $character->id));

                            foreach ($clanPetitions as $clanPetition) {
                                $clanPetition->delete();
                            }

                            foreach ($members as $member) {
                                if ((int) $member->id === (int) $characterLeader->id) {
                                    continue;
                                }

                                $notificationToMember = new Notifications();

                                $notificationToMember->idCharacter = $member->id;
                                $notificationToMember->content = Yii::t('app', 'A new member ("{name}") has been accepted into the clan!', array('{name}' => $character->usarname));

                                $notificationToMember->save();
                            }

                            $character->idGroup = $characterLeader->idGroup;
                            $character->save();

                            $notificationToCharacter = new Notifications();

                            $notificationToCharacter->idCharacter = $character->id;
                            $notificationToCharacter->content = Yii::t('app', 'You have been accepted into "{name}" clan!', array('{name}' => $clan->name));

                            $notificationToCharacter->save();
                        } else {
                            $notification->content = Yii::t('app', 'Error: There are too many members into the clan, you can not accept new members');
                        }
                    } else {
                        $notification->content = Yii::t('app', 'Error: You are not the clan leader!. Only clan leaders can accept new members');
                    }
                } else {
                    $notification->content = Yii::t('app', 'Error: That clan does not exist');
                }
            } else {
                $notification->content = Yii::t('app', 'Error: That character does not exist or already has a clan');
            }

            $notification->save();
        }

        $this->redirect(array('site/index'));
    }

    public function actionSendPetitionToJoinClan($id)
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $notification = new Notifications();
        $notification->idCharacter = $character->id;

        if ((int) $character->idGroup === 0) {
            $petition = ClanPetitions::model()->findByAttributes(array('idCharacter' => $character->id, 'idGroup' => (int) $id));

            if (!is_object($petition)) {
                $clan = Clans::model()->findByPk((int) $id);

                if (is_object($clan)) {
                    $members = Characters::model()->findAllByAttributes(array('idGroup' => (int) $id));

                    if (count($members) < Clans::MEMBER_LIMIT) {
                        $notification->content = Yii::t('app', 'You have sent a petition to join into "{name}" clan', array('{name}' => $clan->name));

                        $clanPetition = new ClanPetitions();

                        $clanPetition->idCharacter = $character->id;
                        $clanPetition->idGroup = $clan->id;

                        $clanPetition->save();

                        $notificationToLeader = new Notifications();

                        $notificationToLeader->idCharacter = $clan->idCreator;
                        $notificationToLeader->content = Yii::t('app', 'New petition to join into the clan by the character "{name}"', array('{name}' => $character->username));

                        $notificationToLeader->save();
                    } else {
                        $notification->content = Yii::t('app', 'Error: That clan cannot accept new members because it is full');
                    }
                } else {
                    $notification->content = Yii::t('app', 'Error: That clan does not exist');
                }
            } else {
                $notification->content = Yii::t('app', 'Error: You have already sent a petition to that clan, wait for answer');
            }
        } else {
            $notification->content = Yii::t('app', 'Error: You already are in a clan');
        }

        $notification->save();

        $this->redirect(array('site/index'));
    }

    public function actionNpc($name)
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $npc = Npcs::model()->findByAttributes(array('name' => $name));
        $itemsToSell = Items::model()->findAllByAttributes(array('npcSells' => (int) $npc->id));
        $quests = Quests::model()->findAllByAttributes(array('idNpc' => (int) $npc->id));

        Events::fire(Events::TALK_TO_NPC_EVENT);

        $this->render('npc', array(
            'character'     => $character,
            'npc'           => $npc,
            'itemsToSell'   => $itemsToSell,
            'quests'        => $quests,
        ));
    }

    public function actionClaimQuestReward($idQuest)
    {
        $quest = Quests::model()->findByPk((int) $idQuest);

        if (is_object($quest)){
            $questProcess = QuestsProcess::model()->findByAttributes(array(
                'idQuest'       => (int) $idQuest,
                'idCharacter'   => (int) Yii::app()->user->getId(),
            ));

            if (is_object($questProcess)) {
                $class = str_replace(' ', '', $quest->name);
                $reward = $class::reward();

                $questProcess->process = 'FINISHED';
                $questProcess->save();
            }
        }

        $this->redirect(array('site/index'));
    }

    public function actionAcceptQuest($id)
    {
        $id = (int) $id;

        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $quest = Quests::model()->findByPk($id);
        $questProcess = QuestsProcess::model()->findByAttributes(array(
            'idQuest'       => (int) $quest->id,
            'idCharacter'   => (int) $character->id,
        ));

        if (!is_object($questProcess)) {
            if ($quest->minLevel <= $character->level) {
                $questProcess = new QuestsProcess();

                $questProcess->idQuest = $quest->id;
                $questProcess->idCharacter = $character->id;

                $questProcess->save();

                $notification = new Notifications();

                $notification->idCharacter = $character->id;
                $notification->content = Yii::t('app', 'You accept the quest: <b>{name}</b>', array('{name}' => $quest->name));

                $notification->save();

                Events::$ACCEPTED_QUEST = $quest;
                Events::fire(Events::ACCEPT_QUEST_EVENT);
            }
        }

        $this->redirect(array('site/index'));
    }

    public function actionBuyItem()
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $item = Items::model()->findByAttributes(array('name' => $_POST['name']));
        $amount = isset($_POST['amount']) ? $_POST['amount'] : 1;
        $ok = false;

        if (is_object($item) && $item->minimumLevel <= $character->level) {
            if (isset($_POST['vipCoin']) && (int) $amount === 1) {
                if ($item->vipPrice <= $character->vipCoins) {
                    $character->vipCoins = $character->vipCoins - $item->vipPrice;
                    $ok = true;
                }
            } else {
                if ($item->goldPrice * $amount <= $character->gold) {
                    $character->gold = $character->gold - $item->goldPrice * $amount;
                    $ok = true;
                }
            }

            if ($ok) {
                if ((int) $item->type === Items::POTION_TYPE) {
                    $itemInInventory = Inventory::model()->findByAttributes(array('idCharacter' => $character->id, 'idItem' => $item->id));

                    if (is_object($itemInInventory)) {
                        /*$itemInInventory->custom = unserialize($itemInInventory->custom);

                        if (!is_array($itemInInventory->custom)) {
                            $itemInInventory->custom = array();
                            $itemInInventory->custom['amount'] = 0;
                        }

                        $itemInInventory->custom['amount'] = $itemInInventory->custom['amount'] + $amount;
                        $itemInInventory->custom = serialize($itemInInventory->custom);*/

                        $itemInInventory->amount = $itemInInventory->amount + $amount;
                        $itemInInventory->save();
                    } else {
                        $inventory = new Inventory();

                        $inventory->idItem = $item->id;
                        $inventory->idCharacter = $character->id;
                        $inventory->amount = $amount;

                        $inventory->save();
                    }
                } else {
                    for ($i = 1; $i <= $amount; ++$i) {
                        $inventory = new Inventory();

                        $inventory->idItem = $item->id;
                        $inventory->idCharacter = $character->id;
                        $inventory->amount = 1;

                        $inventory->save();
                    }
                }

                $notification = new Notifications();

                $notification->idCharacter = $character->id;
                $notification->content = Yii::t('app', 'You buy {amount} {name}', array('{amount}' => $amount, '{name}' => $item->name));

                $notification->save();

                $character->save();
            }
        }

        $this->redirect(array('site/index'));
    }

    public function actionDropItem()
    {
        if (isset($_POST['idItem'])) {
            //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
            $character = Yii::app()->user->character;
            $amount = isset($_POST['amount']) ? $_POST['amount'] : 1;
            $itemInInventory = Inventory::model()->findByAttributes(array('idCharacter' => $character->id, 'idItem' => (int) $_POST['idItem']));

            if (is_object($itemInInventory)) {
                $item = Items::model()->findByPk($itemInInventory->idItem);
                $notification = new Notifications();

                $notification->idCharacter = $character->id;

                if ($amount > 1) {
                    $notification->content = Yii::t('app', '{amount} "{name}" have been deleted from your inventory', array('{amount}' => $amount, '{name}' => $item->name));
                } else {
                    $notification->content = Yii::t('app', '"{name}" has been deleted from your inventory', array('{name}' => $item->name));
                }

                $notification->save();

                if ((int) $item->type === Items::POTION_TYPE) {
                    //$itemInInventory->custom = unserialize($itemInInventory->custom);

                    //if ($itemInInventory->custom['amount'] - $amount <= 0) {
                    if ($itemInInventory->amount - $amount <= 0) {
                        $itemInInventory->delete();
                    } else {
                        /*$itemInInventory->custom['amount'] = $itemInInventory->custom['amount'] - $amount;
                        $itemInInventory->custom = serialize($itemInInventory->custom);*/
                        $itemInInventory->amount = $itemInInventory->amount - $amount;
                        $itemInInventory->save();
                    }
                } else {
                    $itemInInventory->delete();
                }
            }
        }

        $this->redirect(array('site/index'));
    }

    public function actionSaveWeaponInInventory()
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $weapon = new Inventory();

        $weapon->idCharacter = $character->id;
        $weapon->idItem = $character->idWeapon;
        $weapon->amount = 1;
        $weapon->custom = $character->weaponCustom;
        $weapon->save();

        $character->idWeapon = 0;
        $character->save();

        $this->redirect(array('site/index'));
    }

    public function actionSaveShieldInInventory()
    {
        //$character = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $character = Yii::app()->user->character;
        $shield = new Inventory();

        $shield->idCharacter = $character->id;
        $shield->idItem = $character->idShield;
        $shield->amount = 1;
        $shield->custom = $character->shieldCustom;
        $shield->save();

        $character->idShield = 0;
        $character->save();

        $this->redirect(array('site/index'));
    }

    public function actionUseConsumable()
    {
        $idConsumable = isset($_POST['idItem']) ? (int) $_POST['idItem'] : null;
        $amount = isset($_POST['amount']) ? (int) $_POST['amount'] : 1;

        $character = Yii::app()->user->character;
        $consumableInInventory = Inventory::model()->findByAttributes(array('idCharacter' => $character->id, 'idItem' => $idConsumable));
        //$consumableInInventory->custom = unserialize($consumableInInventory->custom);

        if (is_object($consumableInInventory)) {
            $item = Items::model()->findByPk($consumableInInventory->idItem);

            $notification = new Notifications();
            $notification->idCharacter = $character->id;
            if (is_object($item)) {
                if ((int) $item->type === Items::POTION_TYPE) {
                    //if ($consumableInInventory->custom['amount'] >= $amount) {
                    if ($consumableInInventory->amount >= $amount) {
                        //if ($consumableInInventory->custom['amount'] - $amount <= 0) {
                        if ($consumableInInventory->amount - $amount <= 0) {
                            $consumableInInventory->delete();
                        } else {
                            /*$consumableInInventory->custom['amount'] = $consumableInInventory->custom['amount'] - $amount;
                            $consumableInInventory->custom = serialize($consumableInInventory->custom);*/
                            $consumableInInventory->amount = $consumableInInventory->amount - $amount;
                            $consumableInInventory->save();
                        }

                        $sameEffect = Effects::model()->findByAttributes(array('idCharacter' => $character->id, 'idItem' => $idConsumable));

                        if (is_object($sameEffect)) {
                            $sameEffect->amount = $sameEffect->amount + $amount;
                            $sameEffect->save();
                        } else {
                            $timer = new Timers();

                            $timer->idCharacter = $character->id;
                            $timer->description = $item->name;
                            $timer->ends = time() + ($item->timeEffect);
                            $timer->className = 'ConsumableEnds';

                            $timer->save();

                            $effect = new Effects();

                            $effect->idCharacter = $character->id;
                            $effect->idTimer = $timer->id;
                            $effect->idItem = $idConsumable;
                            $effect->amount = $amount;

                            $effect->save();
                        }

                        $notification->icon = 'armas/' . $item->id . '.jpg';
                        $notification->content = Yii::t('app', 'You have been used {amount} of "{name}"', array('{amount}' => $amount, '{name}' => $item->name));
                    } else {
                        $notification->content = Yii::t('app', 'Error: You do not have that amount of "{name}"', array('{name}' => $item->name));
                    }
                } else {
                    $notification->content = Yii::t('app', 'Error: That item is not an a consumable');
                }
            } else {
                $notification->content = Yii::t('app', 'Error: That consumable does not exist');
            }

            $notification->save();
        }

        $this->redirect(array('site/index'));
    }

    public function actionEquipWeapon($idWeapon)
    {
        $idWeapon = (int) $idWeapon;

        //$user = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $user = Yii::app()->user->character;
        $weaponInInventory = Inventory::model()->findByAttributes(array('idCharacter' => $user->id, 'idItem' => $idWeapon));
        $item = Items::model()->findByPk($idWeapon);

        if (is_object($weaponInInventory) && is_object($item) && (int) $item->type === Items::WEAPON_TYPE) {
            if ($user->idWeapon > 0) {
                $actualWeapon = new Inventory();
                
                $actualWeapon->idCharacter = $user->id;
                $actualWeapon->idItem = $user->idWeapon;
                $actualWeapon->amount = 1;
                $actualWeapon->custom = $user->weaponCustom;
                
                $actualWeapon->save();
            }

            $user->idWeapon = $weaponInInventory->idItem;
            $user->weaponCustom = $weaponInInventory->custom;

            $user->save();
            $weaponInInventory->delete();

            Events::fire(Events::CHANGE_WEAPON_EVENT);
        }

        $this->redirect(array('site/index'));
    }

    public function actionEquipShield($idShield)
    {
        $idShield = (int) $idShield;

        //$user = Characters::model()->findByPk((int) Yii::app()->user->getId());
        $user = Yii::app()->user->character;
        $shieldInInventory = Inventory::model()->findByAttributes(array('idCharacter' => $user->id, 'idItem' => $idShield));
        $item = Items::model()->findByPk($idShield);

        if (is_object($shieldInInventory) && is_object($item) && (int) $item->type === Items::SHIELD_TYPE) {
            if ($user->idShield > 0) {
                $actualShield = new Inventory();
                
                $actualShield->idCharacter = $user->id;
                $actualShield->idItem = $user->idShield;
                $actualShield->amount = 1;
                $actualShield->custom = $user->shieldCustom;
                
                $actualShield->save();
            }

            $user->idShield = $shieldInInventory->idItem;
            $user->weaponCustom = $shieldInInventory->custom;

            $user->save();
            $shieldInInventory->delete();
        }

        $this->redirect(array('site/index'));
    }

    public function actionError()
    {
        if($error=Yii::app()->errorHandler->error)
        {
            if(Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionContact()
    {
        $model=new ContactForm;
        if(isset($_POST['ContactForm']))
        {
            $model->attributes=$_POST['ContactForm'];
            if($model->validate())
            {
                $name='=?UTF-8?B?'.base64_encode($model->name).'?=';
                $subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
                $headers="From: $name <{$model->email}>\r\n".
                    "Reply-To: {$model->email}\r\n".
                    "MIME-Version: 1.0\r\n".
                    "Content-type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
                Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact',array('model'=>$model));
    }

    public function actionLogin()
    {
        $model = new LoginForm;

        if(isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        } else {
            if(isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                
                if($model->validate() && $model->login()) {
                    $this->redirect(Yii::app()->user->returnUrl);
                }
            }
        }

        $this->render('login', array('model' => $model));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}