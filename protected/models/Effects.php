<?php

class Effects extends CActiveRecord
{
    public $id;
    public $idCharacter;
    public $idTimer;
    public $idItem;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'effects';
    }

    public function primaryKey()
    {
        return 'id';
    }
}