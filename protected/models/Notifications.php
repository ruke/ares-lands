<?php

class Notifications extends CActiveRecord
{
    public $id;
    public $idCharacter;
    public $icon;
    public $content;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'notifications';
    }

    public function primaryKey()
    {
        return 'id';
    }
}