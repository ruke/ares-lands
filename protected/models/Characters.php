<?php

class Characters extends CActiveRecord
{
    public $id;
    public $username;
    public $password;
    public $email;
    public $title;
    public $description;
    public $gender;
    public $registerDate;
    public $ip;
    public $idLanguage;
    public $idServer;
    public $idGroup;
    public $damage;
    public $defense;
    public $armorPenetration;
    public $evasion;
    public $luck;
    public $life;
    public $constitution;
    public $accuracy;
    public $minDamage;
    public $maxDamage;
    public $armor;
    public $idWeapon;
    public $idShield;
    public $idCompanion;
    public $level;
    public $experience;
    public $experienceToNextLevel;
    public $gold;
    public $idRace;
    public $idZone;
    public $defeats;
    public $victories;
    public $vip;
    public $vipTo;
    public $vipCoins;
    public $idFriend;
    public $showNotifications;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'characters';
    }

    public function primaryKey()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('username, password, email, gender, idLanguage, idServer', 'required'),
            array('username', 'length', 'min' => 3, 'max' => 100),
            array('password', 'length', 'max' => 40),
            array('email', 'email'),
            array('username, email', 'unique', 'className' => 'Characters'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username'      => Yii::t('app', 'Username'),
            'password'      => Yii::t('app', 'Password'),
            'email'         => Yii::t('app', 'E-Mail'),
            'gender'        => Yii::t('app', 'Gender'),
            'idLanguage'    => Yii::t('app', 'Language'),
            'idServer'      => Yii::t('app', 'Server'),
        );
    }

    public function beforeSave()
    {
        if ($this->getIsNewRecord()) {
            $this->password = md5($this->password);
            $this->registerDate = time();
            $this->ip = $_SERVER['REMOTE_ADDR'];
        }
        return true;
    }
}