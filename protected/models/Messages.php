<?php

class Messages extends CActiveRecord
{
    public $id;
    public $idFrom;
    public $idTo;
    public $messageSubject;
    public $messageText;
    public $read;
    public $date;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'messages';
    }

    public function primaryKey()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('idTo, messageSubject, messageText', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'idTo'              => Yii::t('app', 'To'),
            'messageSubject'    => Yii::t('app', 'Message Subject'),
            'messageText'       => Yii::t('app', 'Message Text'),
        );
    }
}