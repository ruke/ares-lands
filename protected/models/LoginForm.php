<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{
    public $username;
    public $password;
    public $idServer;

    private $_identity;

    public function rules()
    {
        return array(
            array('username, password, idServer', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username'  => Yii::t('app', 'Username'),
            'password'  => Yii::t('app', 'Password'),
            'idServer'  => Yii::t('app', 'Server'),
        );
    }

    public function login()
    {
        if($this->_identity === null) {
            $this->_identity = new UserIdentity($this->username, $this->password);
        }

        $this->_identity->idServer = $this->idServer;
        $this->_identity->authenticate();

        switch ((int) $this->_identity->errorCode) {
            case UserIdentity::ERROR_NONE:
                Yii::app()->user->login($this->_identity);
                break;
            
            case UserIdentity::ERROR_USERNAME_INVALID:
            case UserIdentity::ERROR_PASSWORD_INVALID:
                $this->addError('', Yii::t('app', 'Incorrect username or password'));
                break;
            
            default:
                $this->addError('', Yii::t('app', 'An error has been occurred, please try again'));
                break;
        }

        return (int) $this->_identity->errorCode === UserIdentity::ERROR_NONE;
    }
}
