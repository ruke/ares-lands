<?php

class Auctions extends CActiveRecord
{
    const SERVER_SELLER_ID = 0;

    public $id;
    public $idSeller;
    public $idItem;
    public $amount;
    public $price;
    public $cycles;
    public $idBuyer;
    public $offer;


    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'auctions';
    }

    public function primaryKey()
    {
        return 'id';
    }
    public function rules()
    {
        return array(
        array('idSeller, idItem, amount, price', 'required'),
        );
    }
}
