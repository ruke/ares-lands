<?php

class Npcs extends CActiveRecord
{
    const NPC_TYPE = 1;

    public $id;
    public $name;
    public $description;
    public $type;
    public $idZone;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'npcs';
    }

    public function primaryKey()
    {
        return 'id';
    }
}