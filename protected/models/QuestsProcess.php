<?php

class QuestsProcess extends CActiveRecord
{
    public $id;
    public $idQuest;
    public $idCharacter;
    public $idTimer;
    public $process;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'questsprocess';
    }

    public function primaryKey()
    {
        return 'id';
    }
}