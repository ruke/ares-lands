<?php

class Items extends CActiveRecord
{
    const WEAPON_TYPE    = 1;
    const SHIELD_TYPE    = 2;
    const COMPANION_TYPE = 3;
    const POTION_TYPE    = 4;
    const BUFF_TYPE      = 5;
    const STONE_TYPE     = 6;

    public $id;
    public $name;
    public $type;
    public $goldPrice;
    public $vipPrice;
    public $minimumLevel;
    public $timeEffect;
    public $damage;
    public $defense;
    public $armorPenetration;
    public $evasion;
    public $luck;
    public $life;
    public $constitution;
    public $accucary;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'items';
    }

    public function primaryKey()
    {
        return 'id';
    }
}