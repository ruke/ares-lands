<?php

class Inventory extends CActiveRecord
{
    public $id;
    public $idCharacter;
    public $idItem;
    public $amount;
    public $custom;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'inventory';
    }

    public function primaryKey()
    {
        return 'id';
    }
}