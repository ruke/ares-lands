<?php

/**
 * 
 * @package RegisterForm
 * @desc Clase/model que se utiliza en el formulario de registro
 * @author Franco Montenegro
 * @version 1.1.0
 * 
 */
class TradeForm extends Trades
{
    public $amount;

    public function rules()
    {
        $childRules =  array(
            array('amount', 'required'),
        );
        
        $parentRules = parent::rules();
        
        return array_merge($childRules, $parentRules);
    }
    
    public function attributeLabels()
	{
        $childLabels = array(
            'amount' => Yii::t('app', 'Cantidad'),
		);
        
        $parentLabels = parent::attributeLabels();
        
        return array_merge($childLabels, $parentLabels);
	}
}
