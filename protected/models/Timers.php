<?php

class Timers extends CActiveRecord
{
    public $id;
    public $idCharacter;
    public $ends;
    public $className;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'timers';
    }

    public function primaryKey()
    {
        return 'id';
    }
}