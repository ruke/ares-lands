<?php

class ClanPetitions extends CActiveRecord
{
    public $id;
    public $idCharacter;
    public $idGroup;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'clanpetitions';
    }

    public function primaryKey()
    {
        return 'id';
    }
}