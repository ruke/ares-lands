<?php

/**
 * 
 * @package RegisterForm
 * @desc Clase/model que se utiliza en el formulario de registro
 * @author Franco Montenegro
 * @version 1.1.0
 * 
 */
class RegisterForm extends Characters
{
    public $passwordRepeat;
    public $emailRepeat;
    public $verifyCode;
    public $acceptTermsAndConditions = false;
    
    public function rules()
    {
        $childRules =  array(
            array('passwordRepeat, emailRepeat, verifyCode', 'required'),
            array(
                'acceptTermsAndConditions', 'compare', 'compareValue' => true, 
                'message' => Yii::t('app', 'You must agree the terms and conditions')
            ),
            array('passwordRepeat', 'compare', 'compareAttribute' => 'password'),
            array('emailRepeat', 'compare', 'compareAttribute' => 'email'),
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
        
        $parentRules = parent::rules();
        
        return array_merge($childRules, $parentRules);
    }
    
    public function attributeLabels()
	{
        $childLabels = array(
            'passwordRepeat'            => Yii::t('app', 'Repeat password'),
            'emailRepeat'               => Yii::t('app', 'Repeat e-mail'),
            'verifyCode'                => Yii::t('app', 'Verify code'),
            'acceptTermsAndConditions'  => Yii::t('app', 'Accept terms and conditions'),
		);
        
        $parentLabels = parent::attributeLabels();
        
        return array_merge($childLabels, $parentLabels);
	}
}
