<?php

class Clans extends CActiveRecord
{
    const MEMBER_LIMIT = 12;

    public $id;
    public $name;
    public $description;
    public $idCreator;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'clans';
    }

    public function primaryKey()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('name, description', 'required'),
            array('name', 'length', 'min' => 3, 'max' => 100),
            array('name', 'unique', 'className' => 'Clans'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'name'          => Yii::t('app', 'Clan name'),
            'description'   => Yii::t('app', 'Clan description'),
        );
    }
}