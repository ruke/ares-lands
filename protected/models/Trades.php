<?php

class Trades extends CActiveRecord
{
    public $id;
    public $idSeller;
    public $idBuyer;
    public $idItem;
    public $price;
    public $custom;
    public $amount;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'trades';
    }

    public function primaryKey()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('idBuyer, idItem, price', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'idBuyer'  => Yii::t('app', 'Character to sell'),
            'idItem'   => Yii::t('app', 'Item to sell'),
            'price'    => Yii::t('app', 'Price'),
        );
    }
}