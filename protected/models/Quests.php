<?php

class Quests extends CActiveRecord
{
    public $id;
    public $idNpc;
    public $name;
    public $description;
    public $minLevel;
    public $needQuests;
    public $idTimer;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'quests';
    }

    public function primaryKey()
    {
        return 'id';
    }
}