<?php

final class Index
{
    private static $_yii;
    private static $_config;

    public static function run($debugMode = true)
    {
        self::$_yii     = $_SERVER['DOCUMENT_ROOT'] . 'BitBucket/AresLands/ares-lands/framework/yii.php';
        self::$_config  = $_SERVER['DOCUMENT_ROOT'] . 'BitBucket/AresLands/ares-lands/protected/config/main.php';

        defined('YII_DEBUG') or define('YII_DEBUG', $debugMode);
        defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

        if (!class_exists('Yii')) {
            require self::$_yii;
        }

        Yii::createWebApplication(self::$_config)->run();
    }
}

Index::run();