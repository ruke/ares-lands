var timers = new Array();

function checkTime(i)
{
	if (i < 10 && (String(i).charAt(0) != '0' || String(i).length < 2)) {
		i = '0' + i;
	}

	return i;
}

function periodic()
{
	for (var i in timers) {
		if (timers[i]['hours'] <= 0 && timers[i]['minutes'] <= 0 && timers[i]['seconds'] <= 0) {
			//$('[name="' + timers[i]['div'] + '"]').text('Finalizado!');
			window.location.reload();
			break;
		}

		timers[i]['seconds']--;

		if (timers[i]['seconds'] < 0) {
			timers[i]['seconds'] = 59;
			timers[i]['minutes']--;
		}

		if (timers[i]['minutes'] < 0) {
			timers[i]['minutes'] = 59;
			timers[i]['hours']--;

		}

		timers[i]['seconds'] = checkTime(timers[i]['seconds']);
		timers[i]['minutes'] = checkTime(timers[i]['minutes']);
		timers[i]['hours'] = checkTime(timers[i]['hours']);

		$('[name="' + timers[i]['div'] + '"]').text(timers[i]['hours'] + ':' + timers[i]['minutes'] + ':' + timers[i]['seconds']);
	}
	setTimeout(periodic, 1000);
}

function newTimer(div, hours, minutes, seconds)
{
	var i = timers.length;

	timers[i] = new Array();
	timers[i]['div'] = div;
	timers[i]['hours'] = hours;
	timers[i]['minutes'] = minutes;
	timers[i]['seconds'] = seconds;
}

$(document).ready(function(){
	periodic();
});