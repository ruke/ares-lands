function CreateToolTip(){
    var e = document.createElement('div');
    e = $(e);
    
    e.addClass('tooltip');
    e.appendTo(document.body);
    
    return e;
}

function HideToolTip(e, tooltip){
    tooltip.removeAttr('style');
    tooltip.html('');
    tooltip.hide();
}

function GetScrollXY() {
    var scrOfX = 0, scrOfY = 0;
    if( typeof( window.pageYOffset ) == 'number' ) {
        //Netscape compliant
        scrOfY = window.pageYOffset;
        scrOfX = window.pageXOffset;
    } else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
        //DOM compliant
        scrOfY = document.body.scrollTop;
        scrOfX = document.body.scrollLeft;
    } else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
        //IE6 standards compliant mode
        scrOfY = document.documentElement.scrollTop;
        scrOfX = document.documentElement.scrollLeft;
    }
    return [ scrOfX, scrOfY ];
}

function CheckViewport(x, y, width, height, node) {
    var offset = node.offset();
    var document_width = $(document).width(),
        document_height = $(document).height();
        
    var scroll_x = GetScrollXY()[0],
        scroll_y = GetScrollXY()[1];

    // Greater than x viewport
    if ((x + width) > document_width)
        x = document_width - width;
        //x = (offset.left - width);

    // Less than x viewport
    if (x < 0)
        x = 15;

    // Greater than y viewport
    if ((y + height) > (scroll_y + document_height))
        y = y - ((y + height) - (scroll_y + document_height));

    // Node on top of viewport scroll
    else if ((offset.top - 100) < scroll_y)
        y = offset.top + node.outerHeight();

    // Less than y viewport scrolled
    else if (y < scroll_y)
        y = scroll_y + 15;

    // Less than y viewport
    if (y < 0)
        y = 15;

    return {
        x: x,
        y: y
    };
}

function TopCenter(width, height, node) {
    var offset = node.offset(),
        nodeWidth = node.outerWidth(),
        x = offset.left + ((nodeWidth / 2) - (width / 2)),
        y = offset.top - height - 5;

    return CheckViewport(x, y, width, height, node);
}

function ShowToolTip(e, tooltip){
    var html = e.attr('tooltip');

    tooltip.html(html);
    
    var width = tooltip.outerWidth(),
        height = tooltip.outerHeight();
        
    var coords = TopCenter(width, height, e);
    
    tooltip
            .css("left", coords.x +"px")
            .css("top",  coords.y + "px")
            .show();
}

$(document).on('ready', function() {
    var tooltip = CreateToolTip().hide();
    
    $('[tooltip]').each(function(){
        if(/[^ ]/i.test($(this).attr('tooltip'))){
            $(this).hover(
                function(){ShowToolTip($(this), tooltip)},
                function(){HideToolTip($(this), tooltip)}
            );

            $(this).click(function(){HideToolTip($(this), tooltip)});        
        }
    })
})