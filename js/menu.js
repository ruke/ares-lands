$(document).on('ready', function() {
    $('[name="menu"]').each(function(){
        var child = $(this).children('div[name="menuInnerContent"]').hide();
        $(this).click(function() {
            if (!child.is(":visible")) {
                child.slideDown();
            } else {
                child.slideUp();
            }
        });
    })
})